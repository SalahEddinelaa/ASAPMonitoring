package asap.monitoring.batch.configuration;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateCustomizer {

	private String username = "laamimech.salah@gmail.com";

	private String password = "Salaheddine19";

	private String url = "https://thegwave.atlassian.net/rest/api/2/search";

	private RestTemplate restTemplate;

	@Bean
	public RestTemplate restTemplate() {

		HttpHost host = new HttpHost(url, 8080, "http");
		restTemplate = new RestTemplate(new HttpRequestFactoryBasicAuth(host));
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));
		return restTemplate;
	}

}
