package asap.monitoring.utilities.jackson.config;

import asap.monitoring.entities.Sprint;

public class DataStructureJSON {
	
	private Long idProjet;
	
	private Long[] idDemandes;
	 
	private Sprint sprint;


	
	DataStructureJSON() {
		super();
	}



	public Long getIdProjet() {
		return idProjet;
	}



	public void setIdProjet(Long idProjet) {
		this.idProjet = idProjet;
	}



	public Long[] getIdDemandes() {
		return idDemandes;
	}



	public void setIdDemandes(Long[] idDemandes) {
		this.idDemandes = idDemandes;
	}



	public Sprint getSprint() {
		return sprint;
	}



	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}
	
	
	
	

}
