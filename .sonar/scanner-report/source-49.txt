package asap.monitoring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Sprint;
import asap.monitoring.service.ASAPDataService;
import asap.monitoring.service.DemandeService;
import asap.monitoring.service.ProjetService;
import asap.monitoring.service.SprintService;



@RestController
@CrossOrigin("*")
public class SprintController {
	
	
	@Autowired
	private SprintService sprintService;
	
	@Autowired
	private DemandeService demandeService;
	
	@Autowired
	private ASAPDataService asapData;
	
	@Autowired
	private ProjetService projetService;
	
	
	
	@GetMapping("/demandes")
	public List<Demande> allDemandes(){
		return this.demandeService.allDemandes();
	}
	
	@GetMapping("/sprints/projet/{idProjet}")
	public List<Sprint> allSprintOfProjet(@PathVariable Long idProjet){
		return this.sprintService.allSprintsOfProjet(idProjet);
	}
	
	
	
	@GetMapping("/sprints/{nomSprint}/create/{nomProjet}")
	public void save(@PathVariable String nomSprint,@PathVariable String nomProjet) {
		this.sprintService.createSprint(nomProjet, nomSprint);
	}
	
	
	
	@DeleteMapping("/sprints/{idSprint}")
	public void deleteSprint(@PathVariable Long idSprint) {
		this.sprintService.deleteSprint(idSprint);
	}
	
	@GetMapping("/sprints/{idSprint}")
	public Sprint getOne(@PathVariable Long idSprint) {
		return this.sprintService.getSprintById(idSprint);
	}
	
	@GetMapping("/sprints/demandes/{idDemande}/responsable/{idResponsable}")
	public void affecterResponsableToDemande(@PathVariable Long idDemande,@PathVariable Long idResponsable) {
		
		this.demandeService.affecterResponsableToDemande(idDemande, idResponsable);
	}

	
}
