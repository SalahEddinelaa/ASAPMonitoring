package asap.monitoring.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;

import asap.monitoring.entities.Demande;

public interface ChartService {

	public Map<LocalDate, Long> traceChart(long idSprint);
	public List<Demande> fetchClosedIssuesFromAPI() throws IOException ;
	public List<Demande> fetchSprintIssues() throws IOException;
	public List<Demande> fetchIssuesFromAPI(String urlQuery) throws IOException ;
	public Long calculateTotalEstimation(List<Demande> issues) ;
}

