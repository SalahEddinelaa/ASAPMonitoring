package asap.monitoring.service;

import org.springframework.web.multipart.MultipartFile;

public interface StorageInterface {
	
	public String StoreFile(MultipartFile file);
	public String GetImagebase64(String imageName);

}
