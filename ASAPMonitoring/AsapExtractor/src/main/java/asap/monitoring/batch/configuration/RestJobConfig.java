package asap.monitoring.batch.configuration;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import asap.monitoring.batch.utilities.IssueProcessor;
import asap.monitoring.batch.utilities.IssueReader;
import asap.monitoring.entities.Demande;

@Configuration
@EnableBatchProcessing
@EnableScheduling
public class RestJobConfig {
	
	private static final String QUERY_INSERT_ISSUE = "INSERT "
			+ "INTO demande( key, description, priority, estimation, summary, status, resolution_date, subtask) "
			+ "VALUES ( :key, :description, :priority, :estimation, :summary, :status, :resolutionDate ,false)"+
			"ON CONFLICT (key) DO UPDATE  SET description = excluded.description, priority = excluded.priority, " +
			" estimation = excluded.estimation, summary = excluded.summary, "
			+ "status = excluded.status, resolution_date = excluded.resolution_date , subtask= excluded.subtask";

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	ItemWriter<Demande> databaseclosedIssuesWriter(DataSource dataSource, NamedParameterJdbcTemplate jdbcTemplate) {
		JdbcBatchItemWriter<Demande> databaseItemWriter = new JdbcBatchItemWriter<>();
		databaseItemWriter.setDataSource(dataSource);
		databaseItemWriter.setJdbcTemplate(jdbcTemplate);

		databaseItemWriter.setSql(QUERY_INSERT_ISSUE);

		ItemSqlParameterSourceProvider<Demande> sqlParameterSourceProvider = issueSqlParameterSourceProvider();
		databaseItemWriter.setItemSqlParameterSourceProvider(sqlParameterSourceProvider);

		return databaseItemWriter;
	}

	@Bean
	Step getIssueStep(ItemReader<Demande> restReader, ItemProcessor<Demande, Demande> restProcessor,
			ItemWriter<Demande> databaseclosedIssuesWriter, StepBuilderFactory stepBuilderFactory) {
		return stepBuilderFactory.get("getIssueStep").<Demande, Demande>chunk(1).reader(restReader)
				.processor(restProcessor).writer(databaseclosedIssuesWriter).build();
	}

	@Bean
	Job getAndSaveIssueJob(@Qualifier("getIssueStep") Step restIssueStep) {
		return jobBuilderFactory.get("getAndSaveIssueJob").incrementer(new RunIdIncrementer()).flow(restIssueStep).end()
				.build();
	}

	@Bean
	ItemSqlParameterSourceProvider<Demande> issueSqlParameterSourceProvider() {
		return new BeanPropertyItemSqlParameterSourceProvider<>();
	}
	
	@Bean
	ItemReader<Demande> restReader() {
		return new IssueReader(restTemplate);
	}

	@Bean
	ItemProcessor<Demande, Demande> restProcessor() {
		return new IssueProcessor();
	}

}
