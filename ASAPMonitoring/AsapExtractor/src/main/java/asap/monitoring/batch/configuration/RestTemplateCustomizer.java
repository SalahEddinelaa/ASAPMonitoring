package asap.monitoring.batch.configuration;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateCustomizer {

	@Value("${usernamejira}")
	private String username;

	@Value("${password}")
	private String password;

	@Value("${asapurl}")
	private String url;

	@Bean
	public RestTemplate restTemplate() {
		HttpHost host = new HttpHost(url, 8080, "http");
		RestTemplate restTemplate = new RestTemplate(new HttpRequestFactoryBasicAuth(host));
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));
		return restTemplate;
	}

}
