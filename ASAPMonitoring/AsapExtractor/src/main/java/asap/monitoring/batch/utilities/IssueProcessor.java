package asap.monitoring.batch.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import asap.monitoring.entities.Demande;

public class IssueProcessor implements ItemProcessor<Demande, Demande> {

	private static final Logger logger = LoggerFactory.getLogger(IssueProcessor.class);

	@Override
	public Demande process(Demande item) throws Exception {
		logger.info("Processing Issues information: {}", item);
        return item;
	}

}
