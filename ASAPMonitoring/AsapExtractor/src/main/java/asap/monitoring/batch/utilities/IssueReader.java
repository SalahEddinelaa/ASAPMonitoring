package asap.monitoring.batch.utilities;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import asap.monitoring.batch.configuration.IssueJobLauncher;
import asap.monitoring.entities.Demande;
import asap.monitoring.service.ChartService;

public class IssueReader implements ItemReader<Demande> {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(IssueJobLauncher.class);

	@Autowired
	private ChartService chartService ;
	
	private int nextIssueIndex;

	public IssueReader(RestTemplate restTemplate) {
		this.nextIssueIndex = 0;
	}

	@Override
	public Demande read() throws Exception {
		List<Demande> issueData = chartService.fetchClosedIssuesFromAPI();
		Demande nextIssue = null;
		LOGGER.info("the next index is "+nextIssueIndex);
		if (nextIssueIndex < issueData.size()) {
			nextIssue = issueData.get(issueData.size()-1-nextIssueIndex);
			nextIssueIndex++;
		}
		LOGGER.info("the next issue Emitted is "+nextIssue);
		return nextIssue;
	}
	
}
