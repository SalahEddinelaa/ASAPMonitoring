package asap.monitoring.controllers;

import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import asap.monitoring.service.ChartService;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class ChartController {
	
	@Autowired
	private ChartService chartService ;
	
	@GetMapping("/chart/{idSprint}")
	public Map<LocalDate, Long> traceChart(@PathVariable("idSprint") long idSprint){
		return chartService.traceChart(idSprint);
	}

}
