package asap.monitoring.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Equipe;
import asap.monitoring.service.EquipeService;
import asap.monitoring.utilities.jackson.config.Request;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class EquipeController {

	@Autowired
	private EquipeService teamService;
	
	@GetMapping("/teams")
	public Page<Equipe> listTeams( Pageable pageable){
		return teamService.listAllByPage(pageable);
	}
	
	@GetMapping("/teams/length")
	public long listTeamsLength(){
		return teamService.getSize();
	}
	
	@GetMapping("/teams/{id}")
	public List<Developpeur> listDevelopers(@PathVariable("id") long teamId){
		return teamService.getDevelopersByTeam(teamId) ;
	}
	
	@PostMapping("/teams")
	public Equipe createTeam(@Valid @RequestBody Equipe team) {
		return teamService.createTeam(team);
	}

	@PutMapping("/teams/{id}")
	public void updateTeam(@PathVariable(value = "id") Long noteId, @Valid @RequestBody Equipe team) {
		teamService.updateTeam(noteId, team);
	}
	
	@DeleteMapping("/teams/{id}")
	public void deleteTeam(@PathVariable(value = "id") Long teamId) {
		teamService.deleteTeam(teamId);
	}

	@PostMapping("/teams/addmember")
	public void addMember(@RequestBody Request request) {
		System.out.println("added developer is "+request.getDeveloppeur());
		teamService.addMemberToTeam(request.getIdTeam(),request.getDeveloppeur())	;
	}

	@DeleteMapping("/team")
	public void deleteMember(@RequestParam("teamId") String idTeam, @RequestParam("userId") String idUser) {
		 teamService.deleteMemberFromTeam(idTeam, idUser);
	}
	
	@RequestMapping(value="/equipes",method=RequestMethod.GET)
	public List<Equipe> allEquipes(){
		return teamService.allEquipes();
	}
	
	@RequestMapping(value="/equipes/projet/{idProjet}",method=RequestMethod.GET)
	public List<Equipe> equipesOfProjet(@PathVariable Long idProjet){
		return teamService.equipesOfProjet(idProjet);
	}
   
	@RequestMapping(value="/equipesNonAffectees/projet/{idProjet}", method=RequestMethod.GET)
    public List<Equipe> equipesNonAffectees(@PathVariable Long idProjet){
	return teamService.equipesNonAffectees(idProjet);
    }
	
	@RequestMapping(value="/desaffectation/{idProjet}/{idEquipe}" , method=RequestMethod.GET)
	public List<Equipe> desaffecterEquipe(@PathVariable Long idProjet , @PathVariable Long idEquipe){
	return teamService.desaffecterEquipe(idProjet, idEquipe);
    }
	
	@CrossOrigin("*")
	@DeleteMapping("/equipes/{idEquipe}")
	public void deleteEquipe(@PathVariable Long idEquipe) {
	this.teamService.deleteEquipe(idEquipe);
	}

}
