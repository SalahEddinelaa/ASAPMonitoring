package asap.monitoring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import asap.monitoring.entities.Application;
import asap.monitoring.entities.Projet;
import asap.monitoring.entities.ScrumMaster;
import asap.monitoring.service.ProjetService;

@RestController
@CrossOrigin("*")
public class ProjetController {
	
	
	@Autowired
	private ProjetService projetService;
	
	
	
	@PostMapping("/projets/{responsable}")
	 public Projet saveProjet(@RequestBody Projet p,@PathVariable Long responsable ) {
		//System.out.println(+responsable);
		 return projetService.saveProjet(p,responsable);
	 }
	
	@PutMapping("/projetEdit/{resp}")
	public Projet updateProjet(@PathVariable Long resp,@RequestBody Projet p ) {
		return projetService.saveProjet(p, resp);
		
	
	}
	@PostMapping("/equipesProjet/{idsEquipes}")
	public Projet affecterEquipeToProjet(@RequestBody Projet p,@PathVariable Long idsEquipes ) {
		return projetService.affecterEquipeToProjet(p, idsEquipes);
	}

	@RequestMapping(value="/projets",method=RequestMethod.GET)
	public List<Projet> allProjets(){
		return projetService.allProjets();
		
	}
	
	
	@RequestMapping(method=RequestMethod.DELETE, value="/projets/{idProjet}")
	public void deleteProjet(@PathVariable("idProjet") Long idProjet)
	{
		projetService.deleteProjet(idProjet);
	}
	
		@GetMapping("/projets/{idProjet}")
		public Projet  getOne(@PathVariable(value = "idProjet") Long idProjet ) {
		    return projetService.getOne(idProjet);
		}
	
		@GetMapping("/projets/scrumMaster/{idProjet}")
		public ScrumMaster getRespOfProjet(@PathVariable(value = "idProjet") Long idProjet ) {
		    return projetService.getRespOfProjet(idProjet);
		}
		
	
		@GetMapping("/projetApps/{idProjet}")
		public List<Application>getAppOfProjet(@PathVariable(value = "idProjet") Long idProjet ) {

		    return projetService.getAppOfProjet(idProjet);
		}
	

	
	@GetMapping("/projets/Of/scrumMaster/{idUser}")
	public List<Projet> projetsOfScrumMaster(@PathVariable Long idUser)
	{
		return this.projetService.projetsOfScrumMaster(idUser);
	}

	
	
	

}
