package asap.monitoring.controllers;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import asap.monitoring.dao.ScrumMasterRepository;
import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.ScrumMaster;
import asap.monitoring.entities.Utilisateur;
import asap.monitoring.service.StorageService;
import asap.monitoring.service.UtilisateurService;

@RestController
@CrossOrigin("*")
public class UtilisateurController {
	
	
	@Autowired
	private UtilisateurService userService;
	
	@Autowired
	private StorageService storage;
	
	@Autowired
	private ScrumMasterRepository smRepository;
	



	
	@GetMapping("/utilisateurs/filter")
	public List<Developpeur> getUsersByNameContaining(@RequestParam String searchTerm, @RequestParam String idTeam) {
		System.out.println("Team Id is " + idTeam);
		return userService.getDevelopersByNameContaining(idTeam, searchTerm);
	}

	
	
	//get all users
	@GetMapping("/utilisateurs")
	public List<Utilisateur> allUsers()
	{
		return this.userService.allusers();
	}
	
	
	// delete user by id
	@DeleteMapping("/utilisateurs/{idUser}")
	public void deleteUser(@PathVariable Long idUser)
	{
		this.userService.deleteUser(idUser);
	}
	
	

	@GetMapping("/utilisateurs/{id}")
	public Utilisateur getUtilisateur(@PathVariable Long id)
	{
		return this.userService.getOne(id);
	}
	
	
	@GetMapping("/utilisateurs/username/{username}")
	public Utilisateur findUserByName(@PathVariable String username) {
		return this.userService.findUserByEmail(username);
	}
	
	
	@GetMapping("utilisateurs/scrumMasters")
	public List<ScrumMaster> allScrumMasters()
	{
		return this.smRepository.findAll();
	}
	
	
	//update user's informations
	@PostMapping("/utilisateurs/profil")
	public void updateUser(@RequestBody Utilisateur u)
	{
       this.userService.updateUser(u);
	}
	
	
	//update user's photo
	@PostMapping(value="utilisateurs/profil/{id}")
	public Utilisateur updateUserPhoto(@RequestParam("file") MultipartFile file,@PathVariable Long id)
	{
		String path=storage.StoreFile(file);
		Utilisateur u=this.userService.updateUserPhoto(path,id);
		return u;
	}
	

	
	@GetMapping("utilisateurs/developpers/projet/{idProjet}")
	public List<Developpeur> developpersOfProject(@PathVariable Long idProjet){
		return this.userService.developpersOfTeam(idProjet);
	}
	
	
	// insert a new user
	@PostMapping(value="/utilisateurs")
	public Utilisateur ajouterUtilisateur(@RequestParam(value="file",required=false) MultipartFile file,@RequestParam("nom") String nom,@RequestParam("prenom") String prenom,
			@RequestParam("revoke") String revoke,@RequestParam("email") String email,@RequestParam("password") String password,@RequestParam("tel") String tel,
			@RequestParam("role") String role)
	{
		             String path=null;
		             if(file!=null) {
		             path=storage.StoreFile(file);
		             }
		             Utilisateur u = new Utilisateur(nom,prenom,revoke,path,email,tel,password);
		             u.setRole(role);
		             Utilisateur resultat=this.userService.saveUser(u);
		        	 if(resultat==null)
		        	 {
		        		 throw new Error("INSERTION_UTILISATEUR_FAILED");
		        	 }
		        	 else {
		        		 return resultat;
		        	 }
		       
	}
	
	

	
	
}
