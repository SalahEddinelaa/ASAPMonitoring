package asap.monitoring.notifications;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

	// The SimpMessagingTemplate is used to send Stomp over WebSocket messages.
	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	public void notify(Notification notification, String username) {
		messagingTemplate.convertAndSendToUser(username, "/app/topic", notification);
		return;
	}

}
