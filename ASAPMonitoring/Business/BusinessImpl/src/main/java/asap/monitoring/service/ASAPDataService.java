package asap.monitoring.service;

import java.util.List;

import asap.monitoring.entities.Sprint;
import asap.monitoring.utilities.jackson.config.Issue;

public interface ASAPDataService {
	
	public List<Issue> getDataFromASAP(String sprint,String projet);
	public Sprint CastIssueToDemande(List<Issue> issues,String projet);

}
