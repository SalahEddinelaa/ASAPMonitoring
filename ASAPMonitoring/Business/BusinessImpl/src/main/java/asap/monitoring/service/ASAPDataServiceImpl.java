package asap.monitoring.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import asap.monitoring.entities.Demande;
import asap.monitoring.entities.SousDemande;
import asap.monitoring.entities.Sprint;
import asap.monitoring.utilities.jackson.config.DataResponse;
import asap.monitoring.utilities.jackson.config.Issue;

@Service
public class ASAPDataServiceImpl implements ASAPDataService{
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	ProjetService projetService;
	
	
//	@Bean
//	RestTemplate rest(RestTemplateBuilder restTemplateBuilder) {
//		
////		CredentialsProvider credsProvider = new BasicCredentialsProvider();
////		credsProvider.setCredentials( new AuthScope("proxyHost",125), new UsernamePasswordCredentials("proxyUser", "proxyPass") );
////		HttpClientBuilder clientBuilder = HttpClientBuilder.create();
////
////		clientBuilder.useSystemProperties();
////		clientBuilder.setProxy(new HttpHost("proxyHost", 125));
////		clientBuilder.setDefaultCredentialsProvider(credsProvider);
////		clientBuilder.setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy());
////
////		CloseableHttpClient client = clientBuilder.build();
////        
////		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
////		factory.setHttpClient(client);
//         
//	     RestTemplate rest=restTemplateBuilder.basicAuthorization("laamimech.salah@gmail.com","Salaheddine19").build();
//	     //rest.setRequestFactory(factory);
//	     return rest;
//	}
	
	public List<Issue> getDataFromASAP(String numeroSprint,String projet){
		String sprint="'Sprint "+numeroSprint+"'";
		List<Issue> data=restTemplate.getForObject("http://thegwave1.atlassian.net/rest/api/2/search?jql=project='"+projet+"' AND sprint="+sprint+" AND issuetype not in (Sub-Task) &fields=id,summary,status,priority,customfield_10016,subtasks,components,customfield_10020,description",DataResponse.class).getIssues();
		return data;
	}
	
	
	public Sprint CastIssueToDemande(List<Issue> issues,String projet){
		List<Demande> demandes=new ArrayList<Demande>();
		List<SousDemande> subtasks=new ArrayList<>();
		SousDemande subtask=null;
		Issue issue=null;
		Demande demande=null;
		Sprint s=issues.get(0).getSprintFields();
		s.setProjet(projetService.findBynomProjet(projet));
		for(int i=0;i<issues.size();i++) {
			issue=issues.get(i);
			demande=new Demande();
			demande.setEstimation(issue.getFields().getStoryPoint());
			demande.setNomDemande(issue.getFields().getSummary());
			demande.setComponents(issue.getFields().getComponents());
			demande.setPriority(issue.getFields().getPriority().getName());
			demande.setStatus(issue.getFields().getStatus().getName());
			demande.setKey(issue.getKey());
			demande.setSprint(s);
			 for(int j=0;j<issue.getFields().getSubtasks().size();j++) {
				 subtask=new SousDemande();
				 subtask.setKey((issue.getFields().getSubtasks().get(j).getKey()));
				 subtask.setNomDemande(issue.getFields().getSubtasks().get(j).getFields().getSummary());
				 subtasks.add(subtask);
				 demande.getSubtasks().add(subtask);
			 }
			 demandes.add(demande);	
		}
		s.getDemandes().addAll(demandes);
		return s;
	}


}
