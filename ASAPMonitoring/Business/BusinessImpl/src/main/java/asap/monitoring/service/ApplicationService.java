package asap.monitoring.service;


import java.util.List;

import asap.monitoring.entities.Application;
import asap.monitoring.entities.Projet;

public interface ApplicationService {
	
	public Application saveApp(Application app , Long idProjet);
	public List<Application> findApplications();
	public Application getApplication(Long idApplication);
	public void deleteApp(Long idApplication);
	public Projet getProjetofApp(Long idApplication);
	

}
