package asap.monitoring.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asap.monitoring.dao.ApplicationRepository;
import asap.monitoring.dao.ProjetRepository;
import asap.monitoring.entities.Application;
import asap.monitoring.entities.Projet;

@Service
public class ApplicationServiceImpl implements ApplicationService {
	
	@Autowired
	private ApplicationRepository appRepository;
	
	@Autowired
	private ProjetRepository projetRepository;

	@Override
	public Application saveApp(Application app, Long idProjet) {
		 app.setProjet(this.projetRepository.getOne(idProjet));
		 return appRepository.save(app);
	}

	@Override
	public List<Application> findApplications() {
		List<Application> applications = this.appRepository.findAll();
		return applications;
	}

	@Override
	public Application getApplication(Long idApplication) {
		return this.appRepository.getOne(idApplication);
	}

	
	@Override
	public void deleteApp(Long idApplication) {
    Application app= this.appRepository.getOne(idApplication);
    if(app.getProjet()!=null) {
    	app.getProjet().getApplications().remove(app);
    	this.projetRepository.save(app.getProjet());
    }
		this.appRepository.deleteById(idApplication);
	}
	

	@Override
	public Projet getProjetofApp(Long idApplication) {
		Application app=this.appRepository.getOne(idApplication);
		return app.getProjet();
		
	}

}
