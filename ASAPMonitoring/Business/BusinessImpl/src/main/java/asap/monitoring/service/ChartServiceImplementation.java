package asap.monitoring.service;

import java.io.IOException;
import java.sql.Date;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import asap.monitoring.dao.SprintRepository;
import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Sprint;

@Service
public class ChartServiceImplementation implements ChartService {

	private String url = "https://thegwave1.atlassian.net/rest/api/2/search?jql=assignee=currentuser() AND issuetype in standardIssueTypes() "
			+ "AND Sprint in openSprints()" + "AND status changed to Done during (startOfDay() , endOfDay())";

	private String url1 = "https://thegwave1.atlassian.net/rest/api/2/search?jql=assignee=currentuser() AND sprint=";

	private RestTemplate restTemplate;

	private SprintRepository sprintRepository;

	@Autowired
	public ChartServiceImplementation(RestTemplate restTemplate, SprintRepository sprintRepository) {
		this.restTemplate = restTemplate;
		this.sprintRepository = sprintRepository;
	}

	long somme;

	@Override
	public Map<LocalDate, Long> traceChart(long idSprint) {
		TreeMap<LocalDate, Long> data = new TreeMap<LocalDate, Long>();
		Sprint sprint = sprintRepository.getOne(idSprint);
		LocalDate start_date = new LocalDate(sprint.getDate_debut());
		LocalDate fin_date = new LocalDate(sprint.getDate_fin());
		long totalEstimation = calculateTotalEstimation(sprint.getDemandes());
		List<Demande> issues = sprint.getDemandes();
		data.put(start_date, totalEstimation);
		for (LocalDate date = start_date; date.isBefore(fin_date.plusDays(1)); date = date.plusDays(1)) {
			List<Demande> closedDemande = new ArrayList<Demande>();
			if (date.plusDays(1).getDayOfWeek() <= 5) {
				for (Demande demande : issues) {
					if (demande.getResolutionDate() != null) {
						LocalDate closedDate = new LocalDate(demande.getResolutionDate());
						if (closedDate.equals(date))
							closedDemande.add(demande);
					}
				}
				totalEstimation -= calculateTotalEstimation(closedDemande);
				data.put(date.plusDays(1), totalEstimation);
			}
		}
		return data;
	}

	@Override
	public List<Demande> fetchClosedIssuesFromAPI() throws IOException {
		return fetchIssuesFromAPI(url);
	}

	// VERIFY
	@Override
	public List<Demande> fetchSprintIssues() throws IOException {
		return fetchIssuesFromAPI(url1);
	}

	@Override
	public List<Demande> fetchIssuesFromAPI(String urlQuery) throws IOException {
		ResponseEntity<String> response = restTemplate.getForEntity(urlQuery, String.class);
		JsonNode issues = new ObjectMapper().readTree(response.getBody()).path("issues");
		final List<Demande> transformedIssues = new ArrayList<Demande>();

		if (issues.isArray()) {
			for (final JsonNode objNode : issues) {
				String key = objNode.get("key").asText();
				JsonNode fields = objNode.get("fields");
				String issueType = fields.get("issuetype").get("name").asText();
				// System.out.println("component is"+fields.get("components"));
				// System.out.println("project is"+fields.get("project"));
				String description = fields.get("description").asText();
				String summary = fields.get("summary").asText();
				String priority = fields.get("priority").get("name").asText();
				// String dueDate = fields.get("duedate").asText();
				String status = fields.get("status").get("statusCategory").get("key").asText();
				JsonNode storyPointsField = fields.get("customfield_10016");
				long storyPoints = 0;
				if (storyPointsField != null)
					storyPoints = storyPointsField.asLong();
				String resolution = fields.get("resolutiondate").asText();
				DateTimeFormatter parser = ISODateTimeFormat.dateTime();
				Date resolutionDate = new Date(parser.parseDateTime(resolution).toDate().getTime());
				// JsonNode subTasks = fields.get("subtasks");
				// System.out.println(subTasks);
				/*
				 * ASAP long storyPoints = fields.get("customField_15622").asLong(); long
				 * valeurCommercial = fields.get("customfield_15623").asLong();
				 * 
				 * SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm");
				 * df.setTimeZone(TimeZone.getTimeZone("UTC")); try { Date dateLivraison =
				 * df.parse(fields.get("customField_17322").asText()); } catch (ParseException
				 * e) { e.printStackTrace(); }
				 */
				transformedIssues
						.add(new Demande(key, description, priority, storyPoints, summary, status, resolutionDate));
			}
		}
		System.out.println(transformedIssues);
		return transformedIssues;
	}

	@Override
	public Long calculateTotalEstimation(List<Demande> issues) {
		somme = 0;
		issues.forEach(item -> somme += item.getEstimation());
		return somme;
	}

}
