package asap.monitoring.service;

import java.util.List;

import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Developpeur;

public interface DemandeService {
	
	public List<Demande> allDemandes();
	public Developpeur affecterResponsableToDemande(Long iddemande,Long idresponsable);
	public void saveAll(List<Demande> issues);

}
