package asap.monitoring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asap.monitoring.dao.DemandeRepository;
import asap.monitoring.dao.DeveloppeurRepository;
import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Developpeur;
import asap.monitoring.notifications.Notification;
import asap.monitoring.notifications.NotificationService;

@Service
public class DemandeServiceImpl implements DemandeService {

	@Autowired
	private DemandeRepository demandeRepository;

	@Autowired
	private DeveloppeurRepository developperRepository;

	@Autowired
	private NotificationService notificationService;

	@Override
	public List<Demande> allDemandes() {
		List<Demande> demandes = this.demandeRepository.findBySprint(null);
		return demandes;
	}

	@Override
	public Developpeur affecterResponsableToDemande(Long iddemande, Long idresponsable) {
		Demande demande = this.demandeRepository.getOne(iddemande);
		Developpeur developper = null;
		if (idresponsable != 0) {
			developper = this.developperRepository.getOne(idresponsable);
			developper.getTasks().add(demande);
		}
		demande.setResponsable(developper);
		this.demandeRepository.save(demande);
		notificationService.notify(new Notification("hello"),developper.getEmail());
		return developper;
	}

	@Override
	public void saveAll(List<Demande> issues) {
		this.demandeRepository.saveAll(issues);
	}

}
