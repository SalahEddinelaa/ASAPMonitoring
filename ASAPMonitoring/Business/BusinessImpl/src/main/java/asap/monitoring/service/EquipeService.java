package asap.monitoring.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Equipe;

public interface EquipeService {
	Page<Equipe> listAllByPage(Pageable pageable);

	Equipe getTeamById(Long idTeam);

	Equipe createTeam(Equipe team);

	Equipe deleteTeam(Long noteId);

	Equipe updateTeam(Long teamId, Equipe newTeam);

	long getSize();

	Equipe addMemberToTeam(Long teamId, Developpeur u);

	List<Developpeur> getDevelopersByTeam(long teamId);

	void deleteMemberFromTeam(String idTeam, String idUser);

	public List<Equipe> allEquipes();

	public List<Equipe> equipesOfProjet(Long idProjet);

	List<Equipe> equipesNonAffectees(Long idProjet);

	public void deleteEquipe(Long idEquipe);

	List<Equipe> desaffecterEquipe(Long idProjet, Long idEquipe);
}
