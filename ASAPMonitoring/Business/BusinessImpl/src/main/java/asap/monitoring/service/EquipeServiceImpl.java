package asap.monitoring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import asap.monitoring.dao.EquipeRepository;
import asap.monitoring.dao.ProjetRepository;
import asap.monitoring.dao.UtilisateurRepository;
import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Equipe;
import asap.monitoring.entities.Projet;
import asap.monitoring.utilities.exceptions.RessourceNotFoundException;

@Service
@Transactional
public class EquipeServiceImpl implements EquipeService {
	
	private static final String NOT_FOUND = "Team Not Found" ;

	@Autowired
	public UtilisateurRepository userRepository;
	@Autowired
	public EquipeRepository teamRepository;
	@Autowired
	public ProjetRepository projetRepository ;
	@Autowired
	public ProjetService projetService;
	

	@Override
	public Equipe createTeam(Equipe team) {
		return teamRepository.save(team);
	}

	@Override
	public Equipe getTeamById(Long idTeam) {
		Equipe team = teamRepository.findById(idTeam)
				.orElseThrow(() -> new RessourceNotFoundException(NOT_FOUND));
		return team;
	}

	@Override
	public Equipe updateTeam(Long teamId, Equipe newTeam) {
		Equipe team = teamRepository.findById(teamId)
				.orElseThrow(() -> new RessourceNotFoundException(NOT_FOUND));
		team.setNomEquipe(newTeam.getNomEquipe());
		team.setMembers(newTeam.getMembers());
		return teamRepository.save(team);
	}

	@Override
	public Equipe deleteTeam(Long teamId) {
		Equipe team = teamRepository.findById(teamId)
				.orElseThrow(() -> new RessourceNotFoundException(NOT_FOUND));
		for(Projet p : team.getProjets()) {
			projetService.deleteEquipeFromListProjet(p.getIdProjet(), teamId);
		}
		teamRepository.delete(team);
		return team;
	}

	@Override
	public Equipe addMemberToTeam(Long teamId, Developpeur u) {

		Equipe team = teamRepository.findById(teamId)
				.orElseThrow(() -> new RessourceNotFoundException(NOT_FOUND));
		team.addMember(u);
		return teamRepository.save(team);
	}
	
	@Override
	public void deleteMemberFromTeam(String teamId, String userId) {
		long tId = Long.parseLong(teamId.trim()); long uId = Long.parseLong(userId.trim());
		Equipe team = teamRepository.findById(tId)
				.orElseThrow(() -> new RessourceNotFoundException(NOT_FOUND));		
		team.getMembers().removeIf(user -> user.getIdUser().equals(uId));		
		teamRepository.save(team);
	}

	@Override
	public Page<Equipe> listAllByPage(Pageable pageable) {
		return teamRepository.findAll(pageable);
	}

	@Override
	public long getSize() {
		return teamRepository.count();
	}

	@Override
	public List<Developpeur> getDevelopersByTeam(long teamId) {
		Equipe team = teamRepository.findById(teamId)
				.orElseThrow(() -> new RessourceNotFoundException(NOT_FOUND));
		return team.getMembers();
	}
	
	@Override
	public List<Equipe> allEquipes() {
		List<Equipe> equipes= this.teamRepository.findAll();
		for(int i=0;i<equipes.size();i++) {
			equipes.get(i);
		}
		return equipes;
	}

	@Override
	public List<Equipe> equipesOfProjet(Long idProjet) {
		if(this.projetRepository.existsById(idProjet)) {
			Projet projet=this.projetRepository.getOne(idProjet);
			List<Equipe> equipeofprojet=projet.getEquipes();
			
			return equipeofprojet;
		}
		throw new Error("Projet introuvable");
	}
@Override
   public List<Equipe> equipesNonAffectees(Long idProjet){
	   if(this.projetRepository.existsById(idProjet)) {
		   Projet projet = this.projetRepository.getOne(idProjet);
		   List<Equipe> allEquipes=this.allEquipes();
		   List<Equipe> equipeofprojet=projet.getEquipes();
		   allEquipes.removeAll(equipeofprojet);
		   return allEquipes;
	   }
	throw new Error("Projet introuvable");
}

    @Override
    public List <Equipe> desaffecterEquipe(Long idProjet, Long idEquipe){
    	Projet projet = this.projetRepository.getOne(idProjet);
    	Equipe equipe=teamRepository.getOne(idEquipe);
    	List<Equipe> allEquipes = projet.getEquipes();
    	allEquipes.remove(equipe);
    	equipe.getProjets().remove(projet);
    	this.teamRepository.save(equipe);
        //this.projetRepository.save(equipe);
	return allEquipes;
      }
    
    
    @Override
    public void deleteEquipe(Long idEquipe) {
	this.teamRepository.deleteById(idEquipe);
     }
}
