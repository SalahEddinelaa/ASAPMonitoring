package asap.monitoring.service;

import java.util.List;

import asap.monitoring.entities.Application;
import asap.monitoring.entities.Equipe;
import asap.monitoring.entities.Projet;
import asap.monitoring.entities.ScrumMaster;

public interface ProjetService {
	
	public Projet getOne(Long idProjet);
	public Projet saveProjet(Projet p, Long idScrumMaster );
	public List<Projet> allProjets();
	public List<Projet> projetsOfScrumMaster(Long idUser);
	public List<Equipe> equipeOfProject(Long idProjet);
	public Projet affecterEquipeToProjet(Projet projet, Long  idsEquipe );
	public void deleteEquipeFromListProjet(Long idProjet, Long idEquipe);
	public ScrumMaster getRespOfProjet(Long idProjet);
	public List<Application> getAppOfProjet(Long idProjet);
	public void deleteProjet(Long idProjet);
	public Projet findBynomProjet(String nomProjet);

}
