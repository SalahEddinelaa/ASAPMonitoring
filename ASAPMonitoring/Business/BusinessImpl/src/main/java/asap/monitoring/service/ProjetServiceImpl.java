package asap.monitoring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asap.monitoring.dao.EquipeRepository;
import asap.monitoring.dao.ProjetRepository;
import asap.monitoring.dao.ScrumMasterRepository;
import asap.monitoring.dao.UtilisateurRepository;
import asap.monitoring.entities.Application;
import asap.monitoring.entities.Equipe;
import asap.monitoring.entities.Projet;
import asap.monitoring.entities.ScrumMaster;

@Service
public class ProjetServiceImpl implements ProjetService {

	@Autowired
	private ProjetRepository projetRepository;

	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private EquipeRepository equipeRepository;

	@Autowired
	private ScrumMasterRepository smRepository;

	@Override
	public Projet getOne(Long idProjet) {
		return this.projetRepository.getOne(idProjet);
	}

	@Override
	public List<Projet> allProjets() {
		return this.projetRepository.findAll();
	}

	@Override
	public List<Projet> projetsOfScrumMaster(Long idUser) {
		if (this.userRepository.existsById(idUser)) {
			List<Projet> allProjets = this.allProjets();
			List<Projet> dontHaveScrumMaster = new ArrayList<>();
			for (int i = 0; i < allProjets.size(); i++) {
				if (allProjets.get(i).getResponsable() == null) {
					dontHaveScrumMaster.add(allProjets.get(i));
				}
			}
			return dontHaveScrumMaster;
		} else {
			throw new Error("UTILISATEUR_NOT_FOUND");
		}
	}

	@Override
	public List<Equipe> equipeOfProject(Long idProjet) {
		return this.projetRepository.getOne(idProjet).getEquipes();
	}

	@Override
	public Projet saveProjet(Projet p, Long idScrumMaster) {
		p.setResponsable(this.smRepository.getOne(idScrumMaster));
		return projetRepository.save(p);
	}

	@Override
	public Projet affecterEquipeToProjet(Projet projet, Long idsEquipe) {
		Projet p = this.projetRepository.getOne(projet.getIdProjet());
		Equipe e = this.equipeRepository.getOne(idsEquipe);
		p.getEquipes().add(e);
		return this.projetRepository.save(p);
	}

	@Override
	public void deleteEquipeFromListProjet(Long idProjet, Long idEquipe) {
		if (this.projetRepository.existsById(idProjet)) {
			Projet p = this.projetRepository.getOne(idProjet);
			Equipe e = this.equipeRepository.getOne(idEquipe);
			p.getEquipes().remove(e);
			this.projetRepository.save(p);
		}
	}

	@Override
	public ScrumMaster getRespOfProjet(Long idProjet) {
		Projet p = this.projetRepository.getOne(idProjet);
		ScrumMaster sm = p.getResponsable();
		return sm;
	}

	@Override
	public List<Application> getAppOfProjet(Long idProjet) {
		Projet p = projetRepository.getOne(idProjet);
		return p.getApplications();
	}

	@Override
	public void deleteProjet(Long idProjet) {
		Projet p = this.projetRepository.getOne(idProjet);
		if (p.getResponsable() != null) {
			p.getResponsable().getSMprojets().remove(p);
			this.smRepository.save(p.getResponsable());
		}
		projetRepository.deleteById(idProjet);
	}

	@Override
	public Projet findBynomProjet(String nomProjet) {
		return this.projetRepository.findByNomProjet(nomProjet);
	}

}
