package asap.monitoring.service;

import java.util.List;

import asap.monitoring.entities.Sprint;

public interface SprintService {
	
	public List<Sprint> allSprintsOfProjet(Long idProjet);
	public Sprint createSprint(String nomProjet,String nomSprint);
	public void deleteSprint(Long idSprint);
	public Sprint getSprintById(Long idSprint);
	public Boolean isExist(String nomSprint,String nomProjet);

}
