package asap.monitoring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asap.monitoring.dao.DemandeRepository;
import asap.monitoring.dao.SprintRepository;
import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Sprint;

@Service
public class SprintServiceImpl implements SprintService{
	
	@Autowired
	private DemandeRepository demandeRepository;
	
	@Autowired
	private SprintRepository sprintRepository;
	
	@Autowired
	private ASAPDataService asapData;
	


	
	@Override
	public List<Sprint> allSprintsOfProjet(Long idProjet) {
		
		List<Sprint> sprints=this.sprintRepository.findByProjetIdProjet(idProjet);
		sprints.sort((sprint1,sprint2) -> sprint1.getDate_debut().compareTo(sprint2.getDate_debut()));
		return sprints;
	}

	
	public Sprint saveSprint(Sprint sprint) {
		Sprint s= this.sprintRepository.save(sprint);
		return s;
	}
	

	@Override
	public Sprint createSprint(String nomProjet,String nomSprint) {
		
		if(!this.isExist(nomSprint,nomProjet))
		{
		Sprint sprint=this.asapData.CastIssueToDemande(this.asapData.getDataFromASAP(nomSprint,nomProjet),nomProjet);
		return this.sprintRepository.save(sprint);
		}else {
			throw new Error("ERREUR_INSERTION_SPRINT");
		}
		
	}


	public void desaffecterDemandeFromStrint(List<Demande> demandes) {
		
		for(int i=0;i<demandes.size();i++) {
			 demandes.get(i).setSprint(null);
		}
		this.demandeRepository.saveAll(demandes);
	}


	@Override
	public void deleteSprint(Long idSprint) {
		Sprint sprint=this.sprintRepository.getOne(idSprint);
		this.sprintRepository.delete(sprint);
	}


	@Override
	public Sprint getSprintById(Long idSprint) {
		return this.sprintRepository.getOne(idSprint);
	}


	@Override
	public Boolean isExist(String nomSprint,String nomProjet) {
		
		if(this.sprintRepository.findByNomSprintAndProjetNomProjet(nomSprint,nomProjet)!=null)
		{
			throw new Error("SPRINT_ALREADY_EXISTS");
		}
		return false;
	}

}
