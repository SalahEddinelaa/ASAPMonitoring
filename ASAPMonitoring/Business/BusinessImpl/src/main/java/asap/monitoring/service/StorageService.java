package asap.monitoring.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import asap.monitoring.storage.config.UploadPathConfig;


@Service
public class StorageService implements StorageInterface{

	
	@Autowired
	UploadPathConfig uploadConfig;
	
	
	
	@Override
	public String StoreFile(MultipartFile file)
	{
	 if (file.isEmpty()) {
         return null;
     }

     try {
         byte[] bytes = file.getBytes();
         
         String uploadPath=uploadConfig.getUploadPath();

         File uploadDir = new File(uploadPath);
         if (!uploadDir.exists()) {
             uploadDir.mkdir();
         }
         
         Path path = Paths.get(uploadPath + file.getOriginalFilename());

         Files.write(path, bytes);

         return file.getOriginalFilename();

     } catch (IOException e) {
         e.printStackTrace();
         return null;
     }
	}

}
