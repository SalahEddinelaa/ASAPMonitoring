package asap.monitoring.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import asap.monitoring.entities.Utilisateur;

@Service
public class UserDetailsImpl implements UserDetailsService{
	
	@Autowired
	private UtilisateurService userService;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Utilisateur user=this.userService.findUserByEmail(email);
		if(user==null) {
			throw new UsernameNotFoundException("USER_NOT_FOUND");
		}
		
		Collection<GrantedAuthority> roles=new ArrayList<>();
		
		roles.add(new SimpleGrantedAuthority(user.getRole()));
		
		return new User(user.getEmail(), user.getPassword(), roles);
	}

}
