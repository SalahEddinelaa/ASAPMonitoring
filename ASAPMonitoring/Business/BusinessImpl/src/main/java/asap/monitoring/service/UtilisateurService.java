package asap.monitoring.service;

import java.util.List;

import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Utilisateur;

public interface UtilisateurService {

	public List<Utilisateur> allusers();

	public void deleteUser(Long idUser);

	public Utilisateur getOne(Long idUser);

	public Utilisateur saveUser(Utilisateur u);

	public Utilisateur updateUser(Utilisateur u);

	public Utilisateur updateUserPhoto(String nouveauPath, Long idUser);

	public List<Developpeur> developpersOfTeam(Long idProjet);

	public Utilisateur findUserByEmail(String email);

	public List<Developpeur> getDevelopersByNameContaining( String idTeam,String searchTerm);
}
