package asap.monitoring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import asap.monitoring.dao.DemandeRepository;
import asap.monitoring.dao.DeveloppeurRepository;
import asap.monitoring.dao.EquipeRepository;
import asap.monitoring.dao.ScrumMasterRepository;
import asap.monitoring.dao.UtilisateurRepository;
import asap.monitoring.entities.Admin;
import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Equipe;
import asap.monitoring.entities.Projet;
import asap.monitoring.entities.ScrumMaster;
import asap.monitoring.entities.Utilisateur;
import asap.monitoring.utilities.exceptions.RessourceNotFoundException;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

	private final String ROLE_SM = "Scrum Master";
	private final String ROLE_AD = "Administrateur";
	private final String ROLE_DEV = "Developpeur";

	@Autowired
	private UtilisateurRepository userRepository;

	@Autowired
	private ProjetService projetService;

	@Autowired
	private ScrumMasterRepository scrumMasterRepository;

	@Autowired
	private DeveloppeurRepository developperRepository;

	@Autowired
	private EquipeRepository equipeRepository;

	@Autowired
	private DemandeRepository demandeRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<Utilisateur> allusers() {
		List<Utilisateur> users = this.userRepository.findAll();
		return users;
	}

	@Override
	public Utilisateur saveUser(Utilisateur u) {

		String role = u.getRole();
		u.setPassword(bCryptPasswordEncoder.encode(u.getPassword()));

		if (role.contentEquals(this.ROLE_SM)) {
			ScrumMaster sm = new ScrumMaster(u.getNom(), u.getPrenom(), u.getRevoke(), u.getPathPhoto(), u.getEmail(),
					u.getTel(), u.getPassword());
			sm.setRole(this.ROLE_SM);
			sm = this.saveScrumMaster(sm);
			return sm;
		}

		if (role.contentEquals(this.ROLE_DEV)) {
			Developpeur dev = new Developpeur(u.getNom(), u.getPrenom(), u.getRevoke(), u.getPathPhoto(), u.getEmail(),
					u.getTel(), u.getPassword());
			dev.setRole(this.ROLE_DEV);
			dev = this.saveDevelopper(dev);
			return dev;
		}

		if (role.contentEquals(this.ROLE_AD)) {
			Admin admin = new Admin(u.getNom(), u.getPrenom(), u.getRevoke(), u.getPathPhoto(), u.getEmail(),
					u.getTel(), u.getPassword());
			admin.setRole(this.ROLE_AD);
			admin = this.saveAdmin(admin);
			return admin;
		}

		return null;
	}

	public Admin saveAdmin(Admin admin) {
		return this.userRepository.save(admin);
	}

	public ScrumMaster saveScrumMaster(ScrumMaster SM) {
		return this.userRepository.save(SM);
	}

	public Developpeur saveDevelopper(Developpeur developper) {
		return this.userRepository.save(developper);
	}

	@Override
	public void deleteUser(Long idUser) {
		if (this.userRepository.existsById(idUser)) {
			Utilisateur u = this.userRepository.getOne(idUser);
			String role = u.getRole();
			if (role.contentEquals(this.ROLE_SM)) {
				ScrumMaster sm = this.scrumMasterRepository.getOne(idUser);
				List<Projet> projetsOfSM = sm.getSMprojets();
				for (int i = 0; i < projetsOfSM.size(); i++) {
					projetsOfSM.get(i).setResponsable(null);
				}
				sm.setSMprojets(new ArrayList<>());
				this.userRepository.save(sm);

			} else if (role.contentEquals(ROLE_DEV)) {

				Developpeur developper = this.developperRepository.getOne(idUser);
				List<Equipe> equipesOfDevelopper = developper.getMembers();
				List<Demande> demandesOfDevelopper = developper.getTasks();

				for (int i = 0; i < equipesOfDevelopper.size(); i++) {
					equipesOfDevelopper.get(i).getMembers().remove(developper);
				}

				for (int i = 0; i < demandesOfDevelopper.size(); i++) {
					demandesOfDevelopper.get(i).setResponsable(null);
				}

				this.equipeRepository.saveAll(equipesOfDevelopper);
				this.demandeRepository.saveAll(demandesOfDevelopper);

			}

			this.userRepository.delete(u);
		} else {
			throw new Error("UTILISATEUR_NOT_FOUND");
		}
	}

	@Override
	public Utilisateur getOne(Long idUser) {
		if (this.userRepository.existsById(idUser)) {
			return this.userRepository.getOne(idUser);
		} else {
			throw new Error("UTILISATEUR_NOT_FOUND");
		}

	}

	@Override
	public Utilisateur updateUser(Utilisateur u) {

		String oldRole = this.userRepository.getOne(u.getIdUser()).getRole();

		if (!u.getRole().contentEquals(oldRole)) {
			this.deleteUser(u.getIdUser());
			return this.UpdateUserUtility(u);
		} else {
			return this.UpdateUserUtility(u);
		}
	}

	@Override
	public Utilisateur updateUserPhoto(String nouveauPath, Long idUser) {
		Utilisateur u = this.userRepository.getOne(idUser);
		u.setPathPhoto(nouveauPath);
		return this.UpdateUserUtility(u);
	}

	public Utilisateur UpdateUserUtility(Utilisateur u) {
		if (u.getPassword().length() != 0) {
			u.setPassword(bCryptPasswordEncoder.encode(u.getPassword()));
		} else {
			u.setPassword(bCryptPasswordEncoder.encode(this.userRepository.getOne(u.getIdUser()).getPassword()));
		}
		if (u.getRole().contentEquals(this.ROLE_SM)) {
			ScrumMaster sm = new ScrumMaster(u.getNom(), u.getPrenom(), u.getRevoke(), u.getPathPhoto(), u.getEmail(),
					u.getTel(), u.getPassword());
			sm.setIdUser((u.getIdUser()));
			return this.userRepository.save(sm);

		}
		if (u.getRole().contentEquals(this.ROLE_AD)) {
			Admin admin = new Admin(u.getNom(), u.getPrenom(), u.getRevoke(), u.getPathPhoto(), u.getEmail(),
					u.getTel(), u.getPassword());
			admin.setIdUser((u.getIdUser()));
			return this.userRepository.save(admin);
		}
		if (u.getRole().contentEquals(this.ROLE_DEV)) {
			Developpeur dev = new Developpeur(u.getNom(), u.getPrenom(), u.getRevoke(), u.getPathPhoto(), u.getEmail(),
					u.getTel(), u.getPassword());
			dev.setIdUser((u.getIdUser()));
			return this.userRepository.save(dev);
		}
		return null;
	}

	@Override
	public Utilisateur findUserByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	@Override
	public List<Developpeur> developpersOfTeam(Long idProjet) {
		List<Equipe> equipes = this.projetService.equipeOfProject(idProjet);
		List<Developpeur> developpers = new ArrayList<>();
		for (int i = 0; i < equipes.size(); i++) {
			developpers.addAll(equipes.get(i).getMembers());
		}
		return developpers;
	}

	@Override
	public List<Developpeur> getDevelopersByNameContaining(String idTeam, String searchTerm) {
		List<Developpeur> list = userRepository.findByFullNameContainingIgnoreCase(searchTerm);
		if (!idTeam.equals("null")) {
			Equipe team = equipeRepository.findById(Long.valueOf(idTeam))
					.orElseThrow(() -> new RessourceNotFoundException("Team Not Found"));
			List<Developpeur> listToIgnore = team.getMembers();
			list.removeAll(listToIgnore);
		}
		return list;
	}
}
