package asap.monitoring.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import asap.monitoring.entities.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long >{

}
