package asap.monitoring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Sprint;

public interface DemandeRepository extends JpaRepository<Demande,Long>{
	
	List<Demande> findBySprint(Sprint sprint);

}
