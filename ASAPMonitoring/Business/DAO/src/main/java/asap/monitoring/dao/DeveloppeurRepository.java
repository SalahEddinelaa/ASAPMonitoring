package asap.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import asap.monitoring.entities.Developpeur;

public interface DeveloppeurRepository extends JpaRepository<Developpeur,Long>{

}
