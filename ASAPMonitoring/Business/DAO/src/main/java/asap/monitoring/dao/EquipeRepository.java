package asap.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import asap.monitoring.entities.Equipe;

public interface EquipeRepository extends JpaRepository<Equipe,Long>{

}
