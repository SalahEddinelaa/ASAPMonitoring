package asap.monitoring.dao;


import org.springframework.data.jpa.repository.JpaRepository;


import asap.monitoring.entities.Projet;

public interface ProjetRepository extends JpaRepository<Projet,Long>{
   
	public Projet findByNomProjet(String nomProjet);



}
