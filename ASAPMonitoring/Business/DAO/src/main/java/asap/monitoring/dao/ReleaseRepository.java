package asap.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import asap.monitoring.entities.Release;

public interface ReleaseRepository extends JpaRepository<Release,Long>{

}
