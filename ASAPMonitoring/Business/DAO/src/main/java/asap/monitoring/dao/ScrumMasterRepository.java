package asap.monitoring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import asap.monitoring.entities.ScrumMaster;

public interface ScrumMasterRepository extends JpaRepository<ScrumMaster,Long>{

}
