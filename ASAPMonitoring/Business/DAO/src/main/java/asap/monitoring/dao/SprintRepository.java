package asap.monitoring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import asap.monitoring.entities.Sprint;

public interface SprintRepository extends JpaRepository<Sprint,Long>{
	
	public List<Sprint> findByProjetIdProjet(Long id);
	public Sprint findByNomSprintAndProjetNomProjet(String nomSprint,String nomProjet);

}
