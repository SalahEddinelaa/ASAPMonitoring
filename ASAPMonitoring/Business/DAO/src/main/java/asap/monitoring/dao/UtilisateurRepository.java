package asap.monitoring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

	public Utilisateur findByEmail(String email);

	@Query("SELECT u FROM Developpeur u WHERE LOWER(u.nom) LIKE LOWER(CONCAT('%',:searchTearm, '%')) OR LOWER(u.prenom) LIKE LOWER(CONCAT('%',:searchTearm, '%'))")
	List<Developpeur> findByFullNameContainingIgnoreCase(@Param("searchTearm") String searchTerm);
}
