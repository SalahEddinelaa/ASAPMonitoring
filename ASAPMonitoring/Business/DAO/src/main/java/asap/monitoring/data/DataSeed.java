package asap.monitoring.data;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import asap.monitoring.dao.DemandeRepository;
import asap.monitoring.dao.DeveloppeurRepository;
import asap.monitoring.dao.EquipeRepository;
import asap.monitoring.dao.ProjetRepository;
import asap.monitoring.dao.SprintRepository;
import asap.monitoring.dao.UtilisateurRepository;
import asap.monitoring.entities.Admin;
import asap.monitoring.entities.Demande;
import asap.monitoring.entities.Developpeur;
import asap.monitoring.entities.Equipe;
import asap.monitoring.entities.Projet;
import asap.monitoring.entities.ScrumMaster;
import asap.monitoring.entities.SousDemande;
import asap.monitoring.entities.Sprint;

@Component
public class DataSeed {

	
	@Autowired
	private DemandeRepository demandeRepository;
	
	@Autowired
	private SprintRepository sprintRepository;
	
	@Autowired
	private ProjetRepository projetRepository;
	
	@Autowired
	private EquipeRepository equipeRepository;
	
	@Autowired
	private DeveloppeurRepository developperRepository;
	
	@Autowired
	private UtilisateurRepository userRepository;

	Sprint s;
	
	@PostConstruct
	private void seed() throws ParseException {
		//LoadBurndown();

		if(this.projetRepository.count()==0) {
			Projet projet;
			List<Projet> toSave=new ArrayList<Projet>();
			for(int i=0;i<5;i++) {
				projet=new Projet();
				projet.setCodeAP("codeAP"+i);
				projet.setNomProjet("projet "+i);
				toSave.add(projet);
				//projet.getEquipes().add(e);
			}
			this.projetRepository.saveAll(toSave);
		}
	}

	private void LoadBurndown() throws ParseException {
		//demandeRepository.deleteAllInBatch();

		//sprintRepository.deleteAllInBatch();


		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Demande demande0 = new Demande("KEY0", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "done",
				new Date(dateFormat.parse("2018-06-28").getTime()));
		Demande demande2 = new Demande("KEY2", "DESCRIPTION", "35", Long.valueOf("13"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-01").getTime()));
		Demande demande1 = new Demande("KEY1", "DESCRIPTION", "35", Long.valueOf("13"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-03").getTime()));
		Demande demande3 = new Demande("KEY3", "DESCRIPTION", "35", Long.valueOf("5"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-03").getTime()));
		Demande demande4 = new Demande("KEY4", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-04").getTime()));
		Demande demande5 = new Demande("KEY5", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "new",
				new Date(dateFormat.parse("2018-07-05").getTime()));
		Demande demande6 = new Demande("KEY6", "DESCRIPTION", "35", Long.valueOf("5"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-06").getTime()));
		Demande demande7 = new Demande("KEY7", "DESCRIPTION", "35", Long.valueOf("5"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-09").getTime()));
		Demande demande8 = new Demande("KEY8", "DESCRIPTION", "35", Long.valueOf("5"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-09").getTime()));
		Demande demande9 = new Demande("KEY9", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-10").getTime()));
		Demande demande10 = new Demande("KEY10", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-12").getTime()));
		Demande demande11 = new Demande("KEY11", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-15").getTime()));
		Demande demande12 = new Demande("KEY12", "DESCRIPTION", "35", Long.valueOf("8"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-16").getTime()));
		Demande demande13 = new Demande("KEY13", "DESCRIPTION", "35", Long.valueOf("13"), "Summary", "done",
				new Date(dateFormat.parse("2018-07-18").getTime()));
	

		java.util.Date start_date = dateFormat.parse("2018-06-27");
		java.util.Date fin_date = dateFormat.parse("2018-07-18");
		
		List<Demande> issues = new ArrayList<Demande>() {
			{
				add(demande0);
				add(demande1);
				add(demande2);
				add(demande3);
				add(demande4);
				add(demande5);
				add(demande6);
				add(demande7);
				add(demande8);
				add(demande9);
				add(demande10);
				add(demande11);
				add(demande12);
				add(demande13);
		};
		};
		Sprint sprint = new Sprint("Sprint17", new Date(start_date.getTime()), new Date(fin_date.getTime()),issues,(long) 123);
		sprintRepository.save(sprint);
	}
}
