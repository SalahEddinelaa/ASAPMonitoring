package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@DiscriminatorValue("Administrateur")
public class Admin extends Utilisateur{
	
	@OneToMany(mappedBy="createur")
	@JsonIgnore
	private List<Projet> AdminProjets=new ArrayList<Projet>();

	
	public Admin() {
		super();
	}

	public Admin(String nom, String prenom, String revoke, String pathPhoto, String email, String tel,
			String password)
	{
		super(nom,prenom,revoke,pathPhoto,email,tel,password);
	}

	public List<Projet> getAdminProjets() {
		return AdminProjets;
	}


	public void setAdminProjets(List<Projet> adminProjets) {
		AdminProjets = adminProjets;
	}
	
	

}
