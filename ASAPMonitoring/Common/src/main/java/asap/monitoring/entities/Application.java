package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Application {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(columnDefinition= "serial")
	private Long idApplication;
	
	@Column(unique=true)
	private String name;


	@ManyToOne
	private Projet projet;
	
	
	@ManyToMany(mappedBy="components")
	private List<Demande> issues=new ArrayList<Demande>();


	public Application() {
		super();
	}


	public Long getIdApplication() {
		return idApplication;
	}


	public void setIdApplication(Long idApplication) {
		this.idApplication = idApplication;
	}



	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Projet getProjet() {
		return projet;
	}


	public void setProjet(Projet projet) {
		this.projet = projet;
	}


	public List<Demande> getIssues() {
		return issues;
	}


	public void setIssues(List<Demande> issues) {
		this.issues = issues;
	}


	@Override
	public String toString() {
		return "Application [idApplication=" + idApplication + ", name=" + name + ", projet=" + projet + ", issues="
				+ issues + "]";
	}
	

}
