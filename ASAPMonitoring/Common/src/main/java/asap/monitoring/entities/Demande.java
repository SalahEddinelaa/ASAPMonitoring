package asap.monitoring.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="subtask")
@DiscriminatorValue("false")
public class Demande {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idDemande;
	
	private String nomDemande;
	
	@Column(unique=true)
	private String key;
	
	private String description;
	private String priority;
	private float estimation;
	private String summary;
	private String status;
	private Date resolutionDate;

	@ManyToOne
	@JsonIgnore
	private Sprint sprint;
	
	@ManyToOne 
	private Developpeur responsable;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<SousDemande> subtasks=new ArrayList<>();
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JsonIgnore
	private List<Application> components=new ArrayList<>();
	
	@Column(name="subtask", nullable=false, updatable=false, insertable=false)
	protected Boolean subtask;
	
	
	public Demande() {
		super();
	}
	
	public Demande(String key, String description, String priority, long estimation, String summary, String status,Date resolutionDate) {
		super();
		this.key = key;
		this.description = description;
		this.priority = priority;
		this.estimation = estimation;
		this.summary = summary;
		this.status = status;
		this.resolutionDate = resolutionDate;
	}

	public Long getIdDemande() {
		return idDemande;
	}


	public void setIdDemande(Long idDemande) {
		this.idDemande = idDemande;
	}


	public String getNomDemande() {
		return nomDemande;
	}


	public void setNomDemande(String nomDemande) {
		this.nomDemande = nomDemande;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	public Developpeur getResponsable() {
		return responsable;
	}


	public void setResponsable(Developpeur responsable) {
		this.responsable = responsable;
	}

	
	public Boolean getSubtask() {
		return subtask;
	}


	public void setSubtask(Boolean subtask) {
		this.subtask = subtask;
	}


	public List<SousDemande> getSubtasks() {
		return subtasks;
	}


	public void setSubtasks(List<SousDemande> subtasks) {
		this.subtasks = subtasks;
	}
	
	public List<Application> getComponents() {
		return components;
	}


	public void setComponents(List<Application> components) {
		this.components = components;
	}
	
	


	public String getPriority() {
		return priority;
	}


	public void setPriority(String priority) {
		this.priority = priority;
	}


	public float getEstimation() {
		return estimation;
	}


	public void setEstimation(float estimation) {
		this.estimation = estimation;
	}


	public String getSummary() {
		return summary;
	}


	public void setSummary(String summary) {
		this.summary = summary;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getResolutionDate() {
		return resolutionDate;
	}


	public void setResolutionDate(Date resolutionDate) {
		this.resolutionDate = resolutionDate;
	}
	
	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	@Override
	public String toString() {
		return "Demande [idDemande=" + idDemande + ", nomDemande=" + nomDemande + ", key=" + key
				+ ", description=" + description + ", subtask=" + subtask + ", responsable="
				+ responsable + ", subtasks=" + subtasks + ", components=" + components + "]";
	}

	
}
