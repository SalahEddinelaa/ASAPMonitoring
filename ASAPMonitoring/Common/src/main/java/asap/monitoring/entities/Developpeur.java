package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@DiscriminatorValue("Developpeur")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class Developpeur extends Utilisateur{
	
	@ManyToMany(mappedBy="members")
	@JsonIgnore
	private List<Equipe> equipes=new ArrayList<>();
	
	
	@OneToMany(mappedBy="responsable")
	@JsonIgnore
	private List<Demande> tasks=new ArrayList<>();
	
	
	
	public Developpeur() {
		super();
	}
	
	public Developpeur(String nom, String prenom, String revoke, String pathPhoto, String email, String tel,
			String password)
	{
		super(nom,prenom,revoke,pathPhoto,email,tel,password);
	}

	public List<Equipe> getMembers() {
		return equipes;
	}

	public void setMembers(List<Equipe> members) {
		this.equipes = members;
	}

	public List<Demande> getTasks() {
		return tasks;
	}

	public void setTasks(List<Demande> tasks) {
		this.tasks = tasks;
	}



}
