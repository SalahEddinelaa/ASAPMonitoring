package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Equipe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idEquipe;
	
	private String nomEquipe;
	
	@ManyToMany(mappedBy="equipes")
	@JsonIgnore
	private List<Projet> projets=new ArrayList<>();
	
	@ManyToMany
	@JoinTable(name="developper_equipe")
	@JsonBackReference
	private List<Developpeur> members=new ArrayList<>();
	
	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt = new Date();

	public Equipe() {
		super();
	}
	
	public Equipe(String nomEquipe, List<Developpeur> members) {
		super();
		this.createdAt = new Date();
		this.nomEquipe = nomEquipe;
		this.members = members;
	}

	public Long getIdEquipe() {
		return idEquipe;
	}

	public void setIdEquipe(Long idEquipe) {
		this.idEquipe = idEquipe;
	}

	public String getNomEquipe() {
		return nomEquipe;
	}

	public void setNomEquipe(String nomEquipe) {
		this.nomEquipe = nomEquipe;
	}

	
	public List<Projet> getProjets() {
		return projets;
	}

	public void setProjets(List<Projet> projets) {
		this.projets = projets;
	}

	public List<Developpeur> getMembers() {
		return members;
	}

	public void setMembers(List<Developpeur> members) {
		this.members = members;
	}

	@Override
	public String toString() {
		return "Equipe [idEquipe=" + idEquipe + ", nomEquipe=" + nomEquipe + "]";
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void addMember(Developpeur u) {
		this.members.add(u);
	}

	public void deleteMember(Utilisateur u) {
		this.members.remove(u);
	}
	
}
