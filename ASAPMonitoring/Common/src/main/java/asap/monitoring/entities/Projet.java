package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Projet {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idProjet;
	
	
	private String codeAP;
	
	@Column(unique=true)
	private String nomProjet;
	
	private Date date_debut;
	private Date date_fin;
	
	@ManyToOne
	@JsonIgnore
	private ScrumMaster responsable;
	
	@ManyToOne
	private Admin createur;

	
	@ManyToMany
	@JoinTable(name="projet_equipe")
	private List<Equipe> equipes=new ArrayList<>();
	
	
	@OneToMany(mappedBy="projet")
	private List<Sprint> sprints=new ArrayList<>();
	
	@OneToMany(mappedBy="projet")
	private List<Application> applications=new ArrayList<>();
	

	
	public Projet() {
		super();
	}


	public Long getIdProjet() {
		return idProjet;
	}


	public void setIdProjet(Long idProjet) {
		this.idProjet = idProjet;
	}


	public String getCodeAP() {
		return codeAP;
	}


	public void setCodeAP(String codeAP) {
		this.codeAP = codeAP;
	}


	public String getNomProjet() {
		return nomProjet;
	}

;
	public void setNomProjet(String nomProjet) {
		this.nomProjet = nomProjet;
	}


	public Date getDate_debut() {
		return date_debut;
	}


	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}


	public Date getDate_fin() {
		return date_fin;
	}


	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}

	public ScrumMaster getResponsable() {
		return responsable;
	}


	public void setResponsable(ScrumMaster responsable) {
		this.responsable = responsable;
	}


	public Admin getCreateur() {
		return createur;
	}


	public void setCreateur(Admin createur) {
		this.createur = createur;
	}


	public List<Equipe> getEquipes() {
		return equipes;
	}


	public void setEquipes(List<Equipe> equipes) {
		this.equipes = equipes;
	}


	public List<Sprint> getSprints() {
		return sprints;
	}

	
	public void setSprints(List<Sprint> sprints) {
		this.sprints = sprints;
	}


	public List<Application> getApplications() {
		return applications;
	}


	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}


	@Override
	public String toString() {
		return "Projet [idProjet=" + idProjet + ", codeAP=" + codeAP + ", nomProjet=" + nomProjet + ", date_debut="
				+ date_debut + ", date_fin=" + date_fin + "]";
	}
	
	
	
	
	

}
