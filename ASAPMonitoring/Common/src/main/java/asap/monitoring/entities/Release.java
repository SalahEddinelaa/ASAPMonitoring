package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Release {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idRelease;
	
	private String nomRelease;
	private String respMOA;
	private Date date_debut;
	private Date date_fin;
	
	@OneToMany(mappedBy="release")
	private List<Sprint> sprints=new ArrayList<>();
	
	
	public Release() {
		super();
	}

	public Long getIdRelease() {
		return idRelease;
	}

	public void setIdRelease(Long idRelease) {
		this.idRelease = idRelease;
	}

	public String getNomRelease() {
		return nomRelease;
	}

	public void setNomRelease(String nomRelease) {
		this.nomRelease = nomRelease;
	}

	public String getRespMOA() {
		return respMOA;
	}

	public void setRespMOA(String respMOA) {
		this.respMOA = respMOA;
	}

	public Date getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}

	public Date getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public List<Sprint> getSprints() {
		return sprints;
	}

	public void setSprints(List<Sprint> sprints) {
		this.sprints = sprints;
	}


	@Override
	public String toString() {
		return "Release [idRelease=" + idRelease + ", nomRelease=" + nomRelease + ", respMOA=" + respMOA
				+ ", date_debut=" + date_debut + ", date_fin=" + date_fin + "]";
	}
	
	
	
	
	

}
