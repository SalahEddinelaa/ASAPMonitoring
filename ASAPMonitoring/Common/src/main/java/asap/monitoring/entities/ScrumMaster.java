package asap.monitoring.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;



@Entity
@DiscriminatorValue("Scrum Master")
public class ScrumMaster extends Utilisateur{
	
	@OneToMany(mappedBy="responsable")
	private List<Projet> SMprojets=new ArrayList<>();


	public ScrumMaster() {
		super();
	}

	public ScrumMaster(String nom, String prenom, String revoke, String pathPhoto, String email, String tel,
			String password)
	{
		super(nom,prenom,revoke,pathPhoto,email,tel,password);
	}
	
	public List<Projet> getSMprojets() {
		return SMprojets;
	}

	
	public void setSMprojets(List<Projet> sMprojets) {
		SMprojets = sMprojets;
	}

	@Override
	public String toString() {
		
		return super.toString()+"ScrumMaster [SMprojets=" + SMprojets + "]";
	}
	
	

}
