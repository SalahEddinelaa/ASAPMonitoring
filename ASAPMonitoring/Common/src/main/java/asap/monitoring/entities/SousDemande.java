package asap.monitoring.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("true")
public class SousDemande extends Demande{
	
	@ManyToOne
	private Demande demandeMere;

	public SousDemande() {
		super();
	}

	public Demande getDemandeMere() {
		return demandeMere;
	}

	public void setDemandeMere(Demande demandeMere) {
		this.demandeMere = demandeMere;
	}

	@Override
	public String toString() {
		return "SousDemande [demandeMere=" + demandeMere + "]";
	}
	

}
