package asap.monitoring.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Sprint {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSprint;

	private String reference;
	private String nomSprint;
	private Date date_debut;
	private Date date_fin;
	private String statut;
	private Long capacite ;

	@ManyToOne
	private Release release;

	@ManyToOne
	@JsonIgnore
	private Projet projet;

	@OneToMany(fetch = FetchType.EAGER  ,cascade=CascadeType.ALL , mappedBy="sprint")
	private List<Demande> demandes = new ArrayList<>();

	public Sprint() {
		super();
	}

	public Sprint(String nomSprint, Date date_debut, Date date_fin, List<Demande> issues, Long capacite) {
		super();
		this.nomSprint = nomSprint;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.demandes = issues;
		this.capacite = capacite;
	}

	public Long getIdSprint() {
		return idSprint;
	}

	public void setIdSprint(Long idSprint) {
		this.idSprint = idSprint;
	}

	public String getNomSprint() {
		return nomSprint;
	}

	public void setNomSprint(String nomSprint) {
		this.nomSprint = nomSprint;
	}

	public Date getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}

	public Date getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}

	public Release getRelease() {
		return release;
	}

	public void setRelease(Release release) {
		this.release = release;
	}

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}

	public List<Demande> getDemandes() {
		return demandes;
	}

	public void setDemandes(List<Demande> demandes) {
		this.demandes = demandes;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Long getCapacite() {
		return capacite;
	}

	public void setCapacite(Long capacite) {
		this.capacite = capacite;
	}

	@Override
	public String toString() {
		return "Sprint [idSprint=" + idSprint + ", reference=" + reference + ", nomSprint=" + nomSprint
				+ ", date_debut=" + date_debut + ", date_fin=" + date_fin + ", statut=" + statut + ", release="
				+ release + ", projet=" + projet + ", demandes=" + demandes + "]";
	}

}
