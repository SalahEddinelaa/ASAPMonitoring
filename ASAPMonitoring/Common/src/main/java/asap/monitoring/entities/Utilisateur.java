package asap.monitoring.entities;


import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="Role")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class Utilisateur {
	


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idUser;
	
	@NotNull
	@NotEmpty
	protected String nom;
	
	@NotNull
	@NotEmpty
	protected String prenom;

	
	protected String pathPhoto;
	
	@NotNull
	@NotEmpty
	protected String tel;
	
	@NotNull
	@Size(min=10)
	protected String password;
	
	@NotNull
	@Email
	@Column(unique=true)
	protected String email;
	
	@Column(unique=true,nullable=false)
	protected String revoke;
	
	@Column(name="Role", nullable=false, updatable=false, insertable=false)
	protected String Role;
	
	@Transient
	protected String PhotoBase64;
	
	public Utilisateur() {
	}

	public Utilisateur(String nom, String prenom, String revoke, String pathPhoto, String email, String tel,
			String password) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.revoke = revoke;
		this.pathPhoto = pathPhoto;
		this.email = email;
		this.tel = tel;
		this.password = password;
		
	}


	public Long getIdUser() {
		return idUser;
	}


	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getRevoke() {
		return revoke;
	}


	public void setRevoke(String revoke) {
		this.revoke = revoke;
	}


	public String getPathPhoto() {
		return pathPhoto;
	}


	public void setPathPhoto(String pathPhoto) {
		this.pathPhoto = pathPhoto;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return Role;
	}
	
	
	public void setRole(String role) {
		Role = role;
	}

	

	public String getPhotoBase64() {
		return PhotoBase64;
	}


	public void setPhotoBase64(String photoBase64) {
		PhotoBase64 = photoBase64;
	}

	
	@Override
	public String toString() {
		return "Utilisateur [idUser=" + idUser + ", nom=" + nom + ", prenom=" + prenom + ", revoke=" + revoke
				+ ", pathPhoto=" + pathPhoto + ", email=" + email + ", tel=" + tel + ", password=" + password
				+ ", Role=" + Role + ", PhotoBase64=" + PhotoBase64 + "]";
	}


	
	

}
