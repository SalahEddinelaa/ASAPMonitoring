package asap.monitoring.security.utility;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtility {
	
	public static String generateJWT(User user) {
		
		String jwt=Jwts.builder()
				.setSubject(user.getUsername())
				.setExpiration(new Date(System.currentTimeMillis()+SecurityConstantes.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS256,SecurityConstantes.SECRET)
				.claim("roles", user.getAuthorities())
				.compact(); 
		
		return jwt;
	}
	

}
