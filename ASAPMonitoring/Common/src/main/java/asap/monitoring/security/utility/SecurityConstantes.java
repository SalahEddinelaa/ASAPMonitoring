package asap.monitoring.security.utility;

public class SecurityConstantes {
	
	public static final String SECRET="ibmAsapMonitoring@secret123456";
	public static final String TOKEN_PREFIX="Bearer ";
	public static final String HEADER_STRING="Authorization";
	public static final Long EXPIRATION_TIME = new Long("864000000");

}
