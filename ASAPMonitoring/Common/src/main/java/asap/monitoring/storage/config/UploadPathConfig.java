package asap.monitoring.storage.config;

import java.io.File;

import org.springframework.stereotype.Component;

@Component
public class UploadPathConfig {
	
	private final  String UPLOAD_DIRECTORY ="C://Users//IBM_ADMIN//Pictures//test";
	
	
	    public String getUploadPath() {
	    	String uploadPath = UPLOAD_DIRECTORY+ File.separator;
	    	return uploadPath;
	    }

}
