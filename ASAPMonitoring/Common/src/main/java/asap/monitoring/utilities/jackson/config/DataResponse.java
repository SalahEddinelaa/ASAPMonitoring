package asap.monitoring.utilities.jackson.config;

import java.util.ArrayList;
import java.util.List;


public class DataResponse {
	
	public List<Issue> issues=new ArrayList<>();

	public DataResponse() {
		super();
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}
	
	
	
	

}
