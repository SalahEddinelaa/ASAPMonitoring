package asap.monitoring.utilities.jackson.config;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import asap.monitoring.entities.Application;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldsData {
	
	private String summary;
	
	private String description;
	
	private Statut status;
	
	private Priority priority;
	
	 @JsonProperty("customfield_10016")
	private String[] sprint;
	 
	 @JsonProperty("customfield_10020")
	 private float storyPoint;
	 
	private List<Issue> subtasks=new ArrayList<>();
	
	private List<Application> components=new ArrayList<>();
	
	
	
	
	
	
	public FieldsData() {
		super();
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Statut getStatus() {
		return status;
	}

	public void setStatus(Statut status) {
		this.status = status;
	}
	

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	
	
	public List<Issue> getSubtasks() {
		return subtasks;
	}

	public void setSubtasks(List<Issue> subtasks) {
		this.subtasks = subtasks;
	}

	public String[] getSprint() {
		return sprint;
	}

	public void setSprint(String sprint[]) {
		this.sprint = sprint;
	}


	public List<Application> getComponents() {
		return components;
	}

	public void setComponents(List<Application> components) {
		this.components = components;
	}
	

	public float getStoryPoint() {
		return storyPoint;
	}

	public void setStoryPoint(float storyPoint) {
		this.storyPoint = storyPoint;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "FieldsData [summary=" + summary + ", status=" + status + ", priority=" + priority + ", sprint=" + sprint
				+ ", subtasks=" + subtasks + ", components=" + components + "]";
	}
	

}
