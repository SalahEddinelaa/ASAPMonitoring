package asap.monitoring.utilities.jackson.config;


import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import asap.monitoring.entities.Sprint;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue {

    public String key;
	
	public FieldsData fields;

	public Issue() {
		super();
	}

	
	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public FieldsData getFields() {
		return fields;
	}

	public void setFields(FieldsData fields) {
		this.fields = fields;
	}
	
     public Sprint getSprintFields(){
    	 
		Integer index=this.getFields().getSprint()[0].indexOf("[");
		String sprintfields=this.getFields().getSprint()[0].substring(index,this.getFields().getSprint()[0].length()); 
		String s=sprintfields.replace("[","");
		String sFinal=s.replace("]","");
		String[] fields=sFinal.split(",");
		Map<String,String> fieldmapper=new HashMap<>();
		String[] paires;
		for(int i=0;i<fields.length;i++) {
			paires=fields[i].split("=");
			fieldmapper.put(paires[0],paires.length == 1 ? "" : paires[1]);
		}
		DateTimeFormatter parser = ISODateTimeFormat.dateTime();
		Date startDate = new Date(parser.parseDateTime(fieldmapper.get("startDate")).toDate().getTime());
		Date endDate = new Date(parser.parseDateTime(fieldmapper.get("endDate")).toDate().getTime());
		Sprint sprint=new Sprint();
		sprint.setReference(fieldmapper.get("id"));
		sprint.setNomSprint(fieldmapper.get("name"));
		sprint.setDate_debut(startDate);
		sprint.setDate_fin(endDate);
		sprint.setStatut(fieldmapper.get("state"));
        return sprint;
	}

	@Override
	public String toString() {
		return "Issue [id=" + key + ", fields=" + fields + "]";
	}
	
}
