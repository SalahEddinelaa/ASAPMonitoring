package asap.monitoring.utilities.jackson.config;

public class Priority {
	
	private String name;

	public Priority() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Priority [name=" + name + "]";
	}
	
	

}
