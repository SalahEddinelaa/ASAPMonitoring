package asap.monitoring.utilities.jackson.config;

import asap.monitoring.entities.Developpeur;

public class Request {
	private Developpeur developpeur ; 
	private Long idTeam ;
	
	public Developpeur getDeveloppeur() {
		return developpeur;
	}
	
	public void setDeveloppeur(Developpeur developpeur) {
		this.developpeur = developpeur;
	}
	
	public Long getIdTeam() {
		return idTeam;
	}
	
	public void setIdTeam(Long idTeam) {
		this.idTeam = idTeam;
	}
	
	public Request(Developpeur developpeur, Long idTeam) {
		super();
		this.developpeur = developpeur;
		this.idTeam = idTeam;
	}
	
	public Request() {
		
	}
	
}


