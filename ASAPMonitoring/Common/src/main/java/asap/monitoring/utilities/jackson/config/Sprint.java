package asap.monitoring.utilities.jackson.config;

public class Sprint {
	
	private String id;
	
	private String name;
	
	private String endDate;
	
	private String startDate;
	
	private String state;
	
	private String sequence;
	
	public Sprint(String name) {
		this.name = name ;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	@Override
	public String toString() {
		return "Sprint [id=" + id + ", name=" + name + ", endDate=" + endDate + ", startDate=" + startDate + ", state="
				+ state + ", sequence=" + sequence + "]";
	}
	

}
