package asap.monitoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@SpringBootApplication
public class PresentationApplication {

	public static void main(String[] args) {

		SpringApplication.run(PresentationApplication.class, args);
	}
	
	
}
