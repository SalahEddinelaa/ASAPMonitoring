import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgModel } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { AjouterCompteComponent } from "../components/ajouter-compte/ajouter-compte.component";
import { AjouterProjetComponent } from "../components/ajouter-projet/ajouter-projet.component";
import { ListeProjetsComponent } from "../components/liste-projets/liste-projets.component";
import { EditProjetComponent } from "../components/edit-projet/edit-projet.component";
import { AffecterEquipeComponent } from "../components/affecter-equipe/affecter-equipe.component";
import { TeamComponent } from "../components/team/team.component";
import { PagenotfoundComponent } from "../components/pagenotfound/pagenotfound.component";
import { DetailsProjetComponent } from "../components/details-projet/details-projet.component";
import { TeamsTableComponent } from '../components/team/teams-table/teams-table.component';
import { TeamRowComponent } from '../components/team/team-row/team-row.component';
import { BurndownchartComponent } from '../components/burndownchart/burndownchart.component';
import { GestionCompteComponent } from '../components/gestion-compte/gestion-compte.component';
import { AfficherProfilComponent } from '../components/afficher-profil/afficher-profil.component';
import { EditerProfilComponent } from '../components/editer-profil/editer-profil.component';
import { GestionSprintsComponent } from '../components/gestion-sprints/gestion-sprints.component';
import { SidebarComponent } from '../components/sidebar/sidebar.component';
import { LoginComponent } from '../components/login/login.component';


const appRoutes : Routes = [
    {path :"not-found",component:PagenotfoundComponent},
    {path:"login",component:LoginComponent},
    {path :"home",component:SidebarComponent , 
      children : [
        {path:"addaccount",component:AjouterCompteComponent},
        {path:"accounts",component:GestionCompteComponent},
        {path:"profil/:id",component:AfficherProfilComponent},
        {path:"editaccount/:id",component:EditerProfilComponent},
        {path:"sprints/:idprojet",component:GestionSprintsComponent},
        {path:"addprojet",component:AjouterProjetComponent},
        {path:"allproject",component:ListeProjetsComponent},
        {path:"editproject/:id",component:EditProjetComponent},
        {path:"affectTeam/:id",component:AffecterEquipeComponent},
        {path :"manageTeam",component:TeamComponent},
        {path :"addTeam",component:TeamRowComponent},
        {path :"teams",component:TeamsTableComponent},
        {path :"chart/:idSprint/:nomSprint",component:BurndownchartComponent}
]},{path:"",redirectTo:"login",pathMatch:"full"}]
  

   // {path :"**" ,redirectTo: '/not-found' },
  
  
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
  ],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutiingModule { }
