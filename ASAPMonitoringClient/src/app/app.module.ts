import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';  
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import { Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { AjouterCompteComponent } from './components/ajouter-compte/ajouter-compte.component';
import { AjouterProjetComponent } from './components/ajouter-projet/ajouter-projet.component';
import { ListeProjetsComponent } from './components/liste-projets/liste-projets.component';
import { DetailsProjetComponent } from './components/details-projet/details-projet.component';
import { EditProjetComponent } from './components/edit-projet/edit-projet.component';
import { AffecterEquipeComponent } from './components/affecter-equipe/affecter-equipe.component';
import { AfficherProfilComponent } from './components/afficher-profil/afficher-profil.component';
import { EditerProfilComponent } from './components/editer-profil/editer-profil.component';
import { GestionCompteComponent } from './components/gestion-compte/gestion-compte.component';

import { TeamsTableComponent } from './components/team/teams-table/teams-table.component';
import { TeamComponent } from './components/team/team.component';
import { UtilisateurService } from './providers/utilisateur.service';
import { ProjetService } from './providers/projet.service';
import { TeamsService } from './providers/teams.service';
import { HttpErrorHandler } from './providers/http-error-handler.service';
import { TeamRowComponent } from './components/team/team-row/team-row.component';
import {MatTableModule , MatToolbarModule ,MatDialogModule, MatInputModule , MatCardModule , MatButtonModule , MatTabsModule, MatSortModule, MatPaginatorModule , } from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { AppRoutiingModule } from './app-routiing/app-routiing.module';
import { TeamDetailComponent } from './components/team/team-detail/team-detail.component';
import { BurndownchartComponent } from './components/burndownchart/burndownchart.component';
import { BurndownchartService } from './providers/burndownchart.service';
import { ValidationMessageControlComponent } from './components/utility/validation-message-control/validation-message-control.component';
import { AjouterApplicationComponent } from './components/ajouter-application/ajouter-application.component';
import { EditApplicationComponent } from './components/edit-application/edit-application.component';
import { GestionApplicationsComponent } from './components/gestion-applications/gestion-applications.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SprintService } from './providers/sprint.service';
import { ValidationFormsService } from './providers/validation-forms.service';
import { ApplicationService } from './providers/application.service';
import { AuthenticationServiceService } from './providers/authentication-service.service';
import { DetailsDemandeComponent } from './components/details-demande/details-demande.component';
import { GestionSprintsComponent } from './components/gestion-sprints/gestion-sprints.component';
import { LoginComponent } from './components/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    AjouterCompteComponent,
    GestionCompteComponent,
    AfficherProfilComponent,
    EditerProfilComponent,
    ValidationMessageControlComponent,
    GestionSprintsComponent,
    SidebarComponent,
    AjouterApplicationComponent,
    AjouterProjetComponent,
    DetailsProjetComponent,
    EditApplicationComponent,
    EditProjetComponent,
    GestionApplicationsComponent,
    ListeProjetsComponent,
    BurndownchartComponent,
    AffecterEquipeComponent,
    GestionCompteComponent,
    TeamComponent,
    TeamRowComponent,
    TeamsTableComponent,
    TeamDetailComponent,
    PagenotfoundComponent,
    DetailsDemandeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutiingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    NgxPaginationModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatTableModule, MatPaginatorModule, MatSortModule,MatDialogModule,
    MatCardModule ,MatToolbarModule, MatInputModule,MatButtonModule],


  providers: [ProjetService,
    TeamsService, 
    HttpErrorHandler , 
    BurndownchartService,
    ProjetService,
    UtilisateurService,
    ValidationFormsService,
    SprintService,
    AuthenticationServiceService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
