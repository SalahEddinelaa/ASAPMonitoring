import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Projet } from '../../data/entities';
import { ProjetService } from '../../providers/projet.service';
import { TeamsService } from '../../providers/teams.service';



declare let $ :any;
@Component({
  selector: 'app-affecter-equipe',
  templateUrl: './affecter-equipe.component.html',
  styleUrls: ['./affecter-equipe.component.css']
})
export class AffecterEquipeComponent implements OnInit{
  
equipeProjet : any;
idProjet : string;
projet : Projet;
allEquipes : any;
equipesNonAffectees : any;
idsEquipes : string;
affect : any;
equipedesafectee: any;
equipeToDelete : string;
itemsOfPage:number=5;
page: any;





  constructor(private route : ActivatedRoute ,private  equipeService : TeamsService ,private  projetService : ProjetService) {
    
   }

  ngOnInit() {
    this.projetService.getOne(this.route.snapshot.params['id']).
    subscribe(projet=>{
    this.projet=projet;
    this.idProjet = this.route.snapshot.params['id'];
   this.EquipesOfProjet(this.idProjet);
   this.EquipesNonAffectees(this.idProjet);
    });
 
  }

  affecterEquipeToProjet(){
    var equipe=$("#choixequipe").val();
    this.projetService.affecterEquipeToProjet(equipe ,this.projet).subscribe(()=>{
      this.EquipesOfProjet(this.idProjet);
      this.EquipesNonAffectees(this.idProjet);
    });
  }
  desaffecterEquipe(){
    $('#Alert').appendTo("body"); 
    this.equipeService.desaffecterEquipe(this.idProjet,this.equipeToDelete).subscribe(()=>{
      this.EquipesOfProjet(this.idProjet);
      this.EquipesNonAffectees(this.idProjet);
     
    });
  }

  showPopupDelete(idEquipe){
    this.equipeToDelete = idEquipe;
    console.log(this.equipeToDelete);
    $('#Alert').appendTo("body");
  }
EquipesOfProjet(idProjet){
  this.equipeService.getEquipesOfProjet(idProjet).subscribe(data=>{
    this.equipeProjet=data;  
  });
}

 

EquipesNonAffectees(idProjet){
  this.equipeService.getEquipesNonAffectees(idProjet).subscribe(data=>{
    this.equipesNonAffectees=data;
  });

}

}

