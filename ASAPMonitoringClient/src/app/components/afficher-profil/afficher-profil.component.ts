import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Utilisateur } from '../../data/entities';
import { UtilisateurService } from '../../providers/utilisateur.service';



@Component({
  selector: 'app-afficher-profil',
  templateUrl: './afficher-profil.component.html',
  styleUrls: ['./afficher-profil.component.css']
})
export class AfficherProfilComponent implements OnInit {

    user:Utilisateur=new Utilisateur();

  constructor(private userService: UtilisateurService,private route:ActivatedRoute) { 
   this.user.idUser=route.snapshot.params['id'];
  }

  ngOnInit() {
      this.userService.getUtilisateurById(this.user.idUser).subscribe(data=>{
       this.user=data;
      });
  }

}
