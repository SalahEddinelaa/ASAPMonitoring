import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterApplicationComponent } from './ajouter-application.component';

describe('AjouterApplicationComponent', () => {
  let component: AjouterApplicationComponent;
  let fixture: ComponentFixture<AjouterApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
