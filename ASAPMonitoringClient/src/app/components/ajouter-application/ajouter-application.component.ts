import { Component, OnInit } from '@angular/core';
import { Application, Projet} from '../../data/entities';

import { ActivatedRoute, Params } from '@angular/router';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { ProjetService } from '../../providers/projet.service';
import { ApplicationService } from '../../providers/application.service';
import {ValidationFormsService} from '../../providers/validation-forms.service';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';




declare let $:any;

@Component({
  selector: 'app-ajouter-application',
  templateUrl: './ajouter-application.component.html',
  styleUrls: ['./ajouter-application.component.css']
})


export class AjouterApplicationComponent implements OnInit {

  app : Application =new Application();
  listeProjets :any;
  addedApp:Application=new Application();
  project : Projet;
  idApplication : string;
  appForm :FormGroup;
  
  private _success = new Subject<string>();

  staticAlertClosed = true;
  successMessage: string;

  constructor(public  appProvider : ApplicationService ,public  projetProvider : ProjetService , private formBuilder: FormBuilder) {
   
    this.appForm=this.formBuilder.group({
      'nomApplication': ['', Validators.required],
    });
   }

  ngOnInit() {
    this.getAllProjets();
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
  
  }
 
 

  public changeSuccessMessage() {
    this._success.next(` - Application succeffully Added.`);
  }

  addApp(){
    if (this.appForm.dirty && this.appForm.valid){
      this.app.nomApplication=this.appForm.value.nomApplication;
      var projet =$("#choixprojet").val();
    this.appProvider.addApp(this.app,projet).subscribe(data=>{
      this.addedApp=data;
      this.appProvider.getProjetOfApp(this.addedApp.idApplication).subscribe(res=>{
        this.addedApp.projet=res;
        $('#addApp').trigger("reset");
        this._success.next(` Application succeffully Added.`);
      
      });
    });
    
    }
    
  }

  getProjetOfApp(idApplication){
    this.appProvider.getProjetOfApp(idApplication).subscribe(data =>
      {this.project=data;})

  }
  getAllProjets(){
    this.projetProvider.getAllProjets().subscribe(data=>{
      this.listeProjets=data;
    });
   
  }
}
