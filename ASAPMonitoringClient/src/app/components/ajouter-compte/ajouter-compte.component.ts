import { Component, OnInit} from '@angular/core';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { Utilisateur, ScrumMaster, Projet} from '../../data/entities';
import { ProjetService } from '../../providers/projet.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AfficherProfilComponent } from '../afficher-profil/afficher-profil.component';
import { ValidationFormsService } from '../../providers/validation-forms.service';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';




declare let $ :any;

@Component({
  selector: 'app-ajouter-compte',
  templateUrl: './ajouter-compte.component.html',
  styleUrls: ['./ajouter-compte.component.css']
})
export class AjouterCompteComponent implements OnInit{

  currentFileUpload: File;
  allprojetslist:any;
  addedUser=new Utilisateur();
  user:Utilisateur=new Utilisateur();
  userForm: any;

  constructor(private userService: UtilisateurService,private projetService: ProjetService,private route:ActivatedRoute
  ,private formBuilder: FormBuilder) {

    this.userForm = this.formBuilder.group({
      'revoke': ['', Validators.required],
      'nom': ['', Validators.required],
      'prenom': ['', Validators.required],
      'password': ['', [Validators.required,Validators.minLength(10),ValidationFormsService.passwordValidator]],
      'tel': ['', Validators.required],
      'email': ['', [Validators.required,ValidationFormsService.emailValidator]],
      'role':['0',[Validators.required,ValidationFormsService.roleValidator]],
      'photo':[''],
      'repassword':[''],
    });
  }

  ngOnInit() {
  }

  selectFile(event) {
    this.currentFileUpload = event.target.files.item(0);
    $("#chooseFileInput").val(this.currentFileUpload.name);
  }

  AddCompte() {
    
    if (this.userForm.dirty && this.userForm.valid) {
      this.user.revoke=this.userForm.value.revoke;
      this.user.nom=this.userForm.value.nom;
      this.user.prenom=this.userForm.value.prenom;
      this.user.email=this.userForm.value.email;
      this.user.tel=this.userForm.value.tel;
      this.user.password=this.userForm.value.password;
    var idProjet=$("#projet").val();
    this.user.role=$("#role").val();
   this.userService.ToStorage(this.currentFileUpload,this.user).subscribe(data => {
               this.addedUser=data;
               $("#AfterAdding").modal("show");
               this.userForm.reset();  
               $("#AddCompte").trigger("reset");  
               this.currentFileUpload = undefined;      
    });
    
  }
  }

  allProjets()
  {
    this.projetService.allProjets().subscribe(data=>{
      this.allprojetslist=data;
    });
  }


showInputFile(inputId:string){
  $("#"+inputId).trigger('click');
}

}