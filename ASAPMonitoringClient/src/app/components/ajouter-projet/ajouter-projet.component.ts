import { Component, OnInit } from '@angular/core';
import { UtilisateurService} from '../../providers/utilisateur.service';
import { ProjetService} from '../../providers/projet.service';
import { Projet} from '../../data/entities';
import { Utilisateur} from '../../data/entities';
import { FormGroup,FormBuilder, Validators  } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';





declare let $:any;
@Component({
  selector: 'app-ajouter-projet',
  templateUrl: './ajouter-projet.component.html',
  styleUrls: ['./ajouter-projet.component.css']
  
})
export class AjouterProjetComponent implements OnInit {
  listSM:any;
  projet : Projet =new Projet();
  prj : any;
  resp : Utilisateur;
  idProjet:string;
  addedProjet:Projet=new Projet();
  projetForm : FormGroup ;

  constructor(public projetprovider:ProjetService,public userprovider:UtilisateurService ,  private formBuilder: FormBuilder ) {
    this.projetForm=this.formBuilder.group({
      '': ['', Validators.required],
    });
   }

  ngOnInit() {
    this.getAllSM();
  }
  

  getAllSM()
  {
    this.userprovider.listeSM()
    .subscribe(data=>{
        this.listSM=data;
    });
  }

  AddProjet()
  {
    var responsable=$("#choixuser").val();
    this.projetprovider.AddProjet(this.projet,responsable)
    .subscribe(data => {
      this.addedProjet=data;
      this.projetprovider.getRespOfProjet(this.addedProjet.idProjet).subscribe(res =>{
        this.addedProjet.responsable=res;
        $('#addprojet').trigger("reset");
        $("#AfterAdding").modal("show");
      });
    });
  }

 

  getRespOfProjet(idProjet){
    this.projetprovider.getRespOfProjet(idProjet).subscribe(data =>{this.resp=data;})
  }
  
}
