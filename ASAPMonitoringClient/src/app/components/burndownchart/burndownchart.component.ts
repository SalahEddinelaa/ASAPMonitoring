import { Component, OnInit, Injectable, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { BurndownchartService } from '../../providers/burndownchart.service';
import * as Chart from 'chart.js';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-burndownchart',
  templateUrl: './burndownchart.component.html',
  styleUrls: ['./burndownchart.component.css']
})
export class BurndownchartComponent implements AfterViewInit {
  
  labels = [];
  data = [];
  idealLineData = [];
  labelsIndication = [];
  totalEstimation: number ;
  chart: Chart; // This will hold our chart info
  idSprint:number;
  nameSprint: string ;

  constructor(private burndownchart:BurndownchartService, private elementRef: ElementRef , private cd: ChangeDetectorRef ,private router:ActivatedRoute) {
    this.idSprint=router.snapshot.params['idSprint'];
    this.nameSprint=router.snapshot.params['nomSprint'];
   }

  ngAfterViewInit(): void {
        this.constructChart(this.idSprint) ;
        this.cd.detectChanges();
      }
  
   idealFunction(alpha , beta , x){
        return (-alpha/beta)*x+alpha ;
    }

  constructChart(idsprint:number) {
        this.burndownchart.getChartData(idsprint).subscribe(res => {
          this.labels = Object.keys(res);
          this.data =Object.values(res);
          for(var i=0;i<this.labels.length;i++) {
            this.labelsIndication.push(i);
            }
          let alpha = this.data[0];
          let beta = this.labelsIndication[this.labelsIndication.length - 1];
          for(var i =0 ; i<=beta ; i++) {
            this.idealLineData.push(this.idealFunction(alpha,beta,i));
          }          

        let canvas = this.elementRef.nativeElement.querySelector('#canvas');
        this.chart = new Chart(canvas, {
          type: 'line',
          data: {
          labels: this.labels,
            datasets: [
              { 
                data: this.data,
                borderColor: "#0113f9",
                label: 'Actual Tasks Remaining',
                fill: false
              },
              { 
                data: this.idealLineData,
                borderColor: "#ffcc00",
                label: 'Ideal Tasks Remaining',
                fill: false
              },
            ]
          },
          options: {
            legend: {
              display: true,
              position: 'right'

            },
            title: {
              display: true,
              text: 'BurndownChart Of '+this.nameSprint,
              fontColor: '#0113f9'
          },
            scales: {
              xAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'DAYS OF SPRINT',
                  fontColor: '#0113f9'
                }
              }],
              yAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'ESTIMATIONS',
                  fontColor: '#0113f9'
                }
              }],
            }
          }
        });
      });
    }

    }
        