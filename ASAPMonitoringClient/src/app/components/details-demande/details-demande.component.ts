import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { Demande } from '../../data/entities';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { SprintService } from '../../providers/sprint.service';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';


declare let $;
@Component({
  selector: 'app-details-demande',
  templateUrl: './details-demande.component.html',
  styleUrls: ['./details-demande.component.css']
})
export class DetailsDemandeComponent implements OnInit,OnChanges {


  @Input() demande:Demande;
  @Input() idprojet:string;
  developpers:any;
  previousIssue:Demande;


  constructor(private userService:UtilisateurService,private sprintService:SprintService,
    public auth:AuthenticationServiceService) { }

  ngOnInit() {
    this.loadDeveloppers();   
   }

   ngOnChanges(changes: SimpleChanges): void {
    for (let propName in changes) { 
    let change = changes["demande"];
    this.previousIssue = change.previousValue;
    this.demande=change.currentValue;
       }
       this.close();
  }
  

  showSubtaskInfo(subtask:Demande){
     this.previousIssue=this.demande;
     this.demande=subtask;
  }


  showPreviousIssue(){
    this.demande=this.previousIssue;
  }

  close(){
    $(".formChoixResponsable").trigger("reset");
    $('.assigne-collapse').collapse('hide');
  }

  responsableOfTask(){
    var idresponsable=document.getElementsByName("ResponsableChoisi")["0"].value;
    this.sprintService.affectResponsableToDemande(idresponsable,this.demande.idDemande)
      .subscribe(developper=>{
         this.demande.responsable=developper;
         this.close();
      });
    }


  loadDeveloppers(){
    this.userService.getDeveloppersOfProject(this.idprojet).subscribe(data=>{
      this.developpers=data;
    });
  }

  choisirResponsable(){
       $('select[data-role="select2"]').select2({
          width: '100%'
    });
    if(this.demande.responsable!=null){
      $('select[data-role="select2"]').val(this.demande.responsable.idUser).trigger('change');
    }
  }


}
