
import { Component, OnInit } from '@angular/core';
import { ProjetService} from '../../providers/projet.service';
import { Projet, Utilisateur , Application } from '../../data/entities';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-details-projet',
  templateUrl: './details-projet.component.html',
  styleUrls: ['./details-projet.component.css']
})
export class DetailsProjetComponent implements OnInit {

  projet : any;
  resp : Utilisateur;
  idProjet:string;
  allApps :any;

  constructor (private route : ActivatedRoute , public  projetService : ProjetService) {
   }

  ngOnInit()  {
    this.route.queryParams.subscribe(params => {
      console.log(params['id']);
      this.projetService.getOne(params['id']).
      subscribe(projet =>{this.projet=projet;
       this.getRespOfProjet(params['id']);
       this.getAppOfProjet(params['id']);
           
      });
    });
  }

  getRespOfProjet(idProjet){
    this.projetService.getRespOfProjet(idProjet).subscribe(data =>{this.resp=data;})
  }

  getAppOfProjet(idProjet){
    this.projetService.getAppOfProjet(idProjet).subscribe(data=>{
      this.allApps=data;
    })
  }

  
}