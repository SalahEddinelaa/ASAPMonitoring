import { Component, OnInit } from '@angular/core';
import { ProjetService } from '../../providers/projet.service';
import { ApplicationService } from '../../providers/application.service';
import { Router } from '@angular/router';
import { Projet } from '../../data/entities';
import { ActivatedRoute, Params } from '@angular/router';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

import {RouterModule, Routes} from '@angular/router';

declare let $:any;
@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.css']
})
export class EditApplicationComponent implements OnInit {
  app : any;
  prj : Projet;
  listeProjets : any;
  private _success = new Subject<string>();

  staticAlertClosed = true;
  successMessage: string;

  allAppList : any;
  apps : any;


  constructor(public  projetService : ProjetService , public applicationService : ApplicationService , public route : ActivatedRoute) { }

  ngOnInit() {
    this.getAllProjets();
    this.route.queryParams.subscribe(params => {
      this.applicationService.getOne(params['id']).
      subscribe(app => {this.app=app;
      this.getProjetOfApp(params['id']);
  
    });
    });
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
  }
  public changeSuccessMessage() {
    this._success.next(` - Application succeffully Added.`);
  }
    getAllProjets(){
      this.projetService.getAllProjets().subscribe(data=>{
        this.listeProjets=data;
      });
     
    }

    getProjetOfApp(idApplication : string){
      this.applicationService.getProjetOfApp(idApplication).subscribe(data=>{
        this.prj=data;
      })
  
    }

 
   

    Update() {  
      var prj=$("#choixprojet").val();
     
     this.applicationService.addApp(this.app,prj).subscribe(() => {
 
          $('#update').trigger("reset");
          this._success.next(` Application succeffully updated .`);
         

        
        });
   
    }
  
}

