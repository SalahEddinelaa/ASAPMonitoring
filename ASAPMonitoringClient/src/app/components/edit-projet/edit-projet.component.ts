import { Component, OnInit } from '@angular/core';
import { Projet, Utilisateur } from '../../data/entities';
import { ActivatedRoute, Params } from '@angular/router';
import { ProjetService} from '../../providers/projet.service';
import { UtilisateurService } from '../../providers/utilisateur.service';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

declare let $:any;
@Component({
  selector: 'app-edit-projet',
  templateUrl: './edit-projet.component.html',
  styleUrls: ['./edit-projet.component.css']
})
export class EditProjetComponent implements OnInit {
  resp:Utilisateur;
  projet:any;
  listSM : any;
  private _success = new Subject<string>();

  staticAlertClosed = true;
  successMessage: string;

  constructor(private route : ActivatedRoute , public  projetService : ProjetService , public userService : UtilisateurService) { }

  ngOnInit() {
    this.getAllSM();
      this.projetService.getOne(this.route.snapshot.params['id']).
      subscribe(projet => {this.projet=projet;
      this.getRespOfProjet(this.route.snapshot.params['id']);
    });
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
  }
  
  getAllSM()
  {
    this.userService.listeSM()
    .subscribe(data=>{
        this.listSM=data;
    });
  }

  getRespOfProjet(idProjet){
    this.projetService.getRespOfProjet(idProjet).subscribe(data =>{this.resp=data;})
  }


  update() {  
     var resp=$("#choixuser").val();
     console.log(
       this.projet.idProjet
     );
    this.projetService.AddProjet(this.projet,resp).subscribe(() => {
      $('#update').trigger("reset");
      this._success.next(` Application succeffully updated .`);
     
      console.log(this.projet);
    });
   }



}
