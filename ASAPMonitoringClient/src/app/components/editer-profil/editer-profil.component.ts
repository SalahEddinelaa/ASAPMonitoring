import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { ActivatedRoute } from '@angular/router';
import {  Utilisateur, Projet } from '../../data/entities';
import { ProjetService } from '../../providers/projet.service';
import { ValidationFormsService } from '../../providers/validation-forms.service';

declare let $ :any;

@Component({
  selector: 'app-editer-profil',
  templateUrl: './editer-profil.component.html',
  styleUrls: ['./editer-profil.component.css']
})
export class EditerProfilComponent implements OnInit {

  currentFileUpload: File;
  userToEdit=new Utilisateur();
  oldRole:string;
  itemsOfPage:number=5;
  page: any;
  userForm: any;

  constructor(private userService:UtilisateurService,private route:ActivatedRoute,private projetService: ProjetService
    ,private formBuilder: FormBuilder) {      
           this.userForm = this.formBuilder.group({
              'revoke': ['',Validators.required],
              'nom': ['',Validators.required],
              'prenom': ['',Validators.required],
              'password': ['',[Validators.minLength(10),ValidationFormsService.passwordValidator]],
              'tel': ['',Validators.required],
              'email': ['',[Validators.required,ValidationFormsService.emailValidator]],
              'role':['0',[Validators.required,ValidationFormsService.roleValidator]]
            });

    }

  ngOnInit() {
    this.getUserByID(this.route.snapshot.params['id'] ); 
  }


  getUserByID(idUser:string){
    this.userService.getUtilisateurById(idUser).subscribe(data=>{
       this.userToEdit=data;
       this.oldRole=this.userToEdit.role;
       this.Styling(this.userToEdit.role);
       this.userForm.controls['nom'].setValue(this.userToEdit.nom);
       this.userForm.controls['prenom'].setValue(this.userToEdit.prenom);
       this.userForm.controls['revoke'].setValue(this.userToEdit.revoke);
       this.userForm.controls['tel'].setValue(this.userToEdit.tel);
       this.userForm.controls['email'].setValue(this.userToEdit.email);
       this.userForm.controls['role'].setValue(this.userToEdit.role);
    });
   
  }



Styling(role:string)
{
  var style=$(".divImage").attr("class");
  if(role == "Scrum Master")
  {
       var newStyle=style+" "+" border-info";
       $(".divImage").attr("class",newStyle);
  }
  if(role == "Developpeur")
  {
    var newStyle = style+" "+" border-success";
    $(".divImage").attr("class",newStyle);
  }
  if(role == "Administrateur")
  {
    var newStyle = style+" "+" border-danger";
    $(".divImage").attr("class",newStyle);
  }
}


updateUserConfirm()
{
  var role=$("#roleEdit").val();
  this.userToEdit.role=role;
  if(this.oldRole!=role)
  {
   $("#Alert").modal("show");
  }
  else{
    this.userToEdit.revoke=this.userForm.value.revoke;
    this.userToEdit.nom=this.userForm.value.nom;
    this.userToEdit.prenom=this.userForm.value.prenom;
    this.userToEdit.email=this.userForm.value.email;
    this.userToEdit.tel=this.userForm.value.tel;
    this.userToEdit.password=this.userForm.value.password;
    this.updateUser();
  }
}

updateUser()
{
  this.userService.updateUser(this.userToEdit).subscribe(newUser=>{
    this.userToEdit=newUser;
    $("#Success").modal("show");
  });
}

nouveauPhotoChange(event)
{
  this.currentFileUpload=event.target.files.item(0);
  this.userService.updateUserPhoto(this.currentFileUpload,this.userToEdit.idUser)
  .subscribe(res=>{
        this.userToEdit.pathPhoto=res.pathPhoto;
        this.currentFileUpload = undefined
  });
}

showInputFile(inputId:string){
  $("#"+inputId).trigger('click');
}

}