import { Component, OnInit } from '@angular/core';
import { ProjetService } from '../../providers/projet.service';
import { ApplicationService } from '../../providers/application.service';
import {Application, Projet} from '../../data/entities';
import { Router } from '@angular/router';
import {RouterModule, Routes} from '@angular/router';

declare let $ :any;
@Component({
  selector: 'app-gestion-applications',
  templateUrl: './gestion-applications.component.html',
  styleUrls: ['./gestion-applications.component.css']
})
export class GestionApplicationsComponent implements OnInit {

apps : any;
projet : any;
allAppList : any;
allAppSearch : Application[]=[];
itemsOfPage:number=5;
page: any;
idApplication : string;
prj: Projet;
listeProjets : any;
app: any;
appToEdit:Application=new Application();



  constructor(public  projetService : ProjetService , public applicationService : ApplicationService , private router : Router) { }

  ngOnInit() {
    this.getAllApplications();
  }
  getAllApplications(){
    this.applicationService.getAllApplications().subscribe(data=>{
    this.apps=data;
    for(let p of this.apps){

      this.applicationService.getProjetOfApp(p.idApplication).subscribe(data=>{
        p.projet=data;

      });
    }
    this.allAppList=data;
    this.allAppSearch=data;

    });
  }
  getProjetOfApp(idApplication : string){
    this.applicationService.getProjetOfApp(idApplication).subscribe(data=>{
      this.prj=data;
    })

  }
  deleteApp(){
    this.applicationService.deleteApp(this.idApplication).subscribe(()=>{
      this.getAllApplications();
   

  }); 
  }
  getAllProjets(){
    this.projetService.getAllProjets().subscribe(data=>{
      this.listeProjets=data;
    });
   
  }


  Update(idApplication : string){
    this.router.navigate(['EditApplication'],{queryParams : {"id" : idApplication}})

   }

  


  searchAnimation(){ 
    $(".iconesearch").css("border-color","#04B404");
    $(".searchinput").css("border-color","#04B404");
  }

  searchApp(motCle:string)
  {
    var resultat:Application[]=[];
    if(motCle == "")
    {
           this.getAllApplications();
    }
    else{

       for(let app of this.allAppSearch)
       {
         if((app.nomApplication.includes(motCle)))
         {
           resultat.push(app);
         }
       }
       this.allAppList=resultat;
    }
  }
  
  }
