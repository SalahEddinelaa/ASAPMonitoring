import { Component, OnInit} from '@angular/core';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { Utilisateur } from '../../data/entities';
import { Router } from '@angular/router';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';

declare let $ :any;

@Component({
  selector: 'app-gestion-compte',
  templateUrl: './gestion-compte.component.html',
  styleUrls: ['./gestion-compte.component.css']
})
export class GestionCompteComponent implements OnInit  {
  
  allUsersList:any;
  allUserSearch:any;
  userToDelete:string;
  itemsOfPage:number=5;
  page: any;


  constructor(private userService:UtilisateurService,private router: Router,public auth:AuthenticationServiceService) {}

  ngOnInit() { 
   this.allUsers();
  }


  allUsers()
  {
    this.userService.allUsers().subscribe(data=>{
        this.allUsersList=data;
        this.allUserSearch=data;
    });
  }

  deleteUser(){
    this.userService.deleteUser(this.userToDelete).subscribe(()=>{
      this.allUsers();
    });
  }


  searchUser(motCle:string)
  {
    var resultat:Utilisateur[]=[];
    if(motCle == "")
    {
           this.allUsers();
    }
    else{

       for(let user of this.allUserSearch)
       {
         if((user.nom.includes(motCle))||(user.prenom.includes(motCle))||(user.email.includes(motCle))||(user.revoke.includes(motCle))||(user.role.includes(motCle))||(user.tel.includes(motCle)))
         {
           resultat.push(user);
         }
       }
       this.allUsersList=resultat;
    }
  }

  searchAnimation(){ 
    $(".iconesearch").css("border-color","#04B404");
    $(".searchinput").css("border-color","#04B404");
  }

  AgrandirImage(photo:string,nom:string,prenom:string){
    var modal = $("#myModal");
    var modalImg = $("#img01");
    var captionText = $("#caption");
    modal.css("display","block");
    modalImg.attr("src",photo);
    modalImg.attr("alt",prenom+" "+nom);
    captionText.html(modalImg.attr("alt"));
}

dismissImage(){
  var modal = $("#myModal");
  modal.css("display","none");
}


}