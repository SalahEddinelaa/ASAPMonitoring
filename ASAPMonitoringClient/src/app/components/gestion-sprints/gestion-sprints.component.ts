import { Component, OnInit, HostListener } from '@angular/core';
import { SprintService } from '../../providers/sprint.service';
import { Demande, Sprint,Developpeur, Projet } from '../../data/entities';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ValidationFormsService } from '../../providers/validation-forms.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { identifierModuleUrl } from '@angular/compiler';
import { ProjetService } from '../../providers/projet.service';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';


declare let $;
@Component({
  selector: 'app-gestion-sprints',
  templateUrl: './gestion-sprints.component.html',
  styleUrls: ['./gestion-sprints.component.css']
})
export class GestionSprintsComponent implements OnInit {


  sprintsOfProjet:Sprint[]=[];
  nbrOfSprints:number;
  currentDivSprint:string;
  ProjectToPlan:Projet;
  idprojet:string;
  sprintToDelete:string;
  addSprintClicked=false;
  selectedIssue:Demande;
  FormSprint:any;
  itemsOfPage:number=5;
  page: any;



  constructor(private sprintService:SprintService,private formBuilder: FormBuilder
  ,private userService:UtilisateurService,private route:ActivatedRoute,
  public auth:AuthenticationServiceService,private projetService:ProjetService) { 

      this.FormSprint = this.formBuilder.group({
        'sprintName': ['', Validators.required]
      });

     this.idprojet=route.snapshot.params['idprojet'];
  
  }

  ngOnInit() {
    this.projetService.getOne(this.idprojet).subscribe(data=>{
      this.ProjectToPlan=data;
      this.allSprintsOfProjet(this.ProjectToPlan.idProjet);
     });
  }

  allSprintsOfProjet(idProjet:string){
    this.sprintService.allSprintsOfProjet(idProjet).subscribe(data=>{
    this.sprintsOfProjet=data;
    });
  }

  createNewSprintHTML(){
    this.sprintsOfProjet.push(null);
    this.addSprintClicked=true;
  }

  saveSprint(){
    var sprintName=this.FormSprint.value.sprintName;
    this.sprintService.createSprint(sprintName,this.ProjectToPlan.nomProjet).subscribe(

      suc=>{
        this.allSprintsOfProjet(this.idprojet);
      },
      err=>{
        console.log(err.error.message);
      } 
    );
  }


  detailsDemande(d){
    this.selectedIssue=d;
  }
    
      deleteSprint(){
        this.sprintService.deleteSprint(this.sprintToDelete).subscribe(()=>{
          this.allSprintsOfProjet(this.ProjectToPlan.idProjet);
        });
      }

      searchSprint(motCle:string)
      {
        var resultat:Sprint[]=[];
        if(motCle == "")
        {
               this.allSprintsOfProjet(this.ProjectToPlan.idProjet);
        }
        else{
          var demandes;
           for(let sprint of this.sprintsOfProjet)
           {
             demandes=sprint.demandes;
             if((sprint.nomSprint.includes(motCle)))
             {
               resultat.push(sprint);
             }
             for(let d of demandes){
               if(d.nomDemande.includes(motCle)){
                 resultat.push(sprint);
                 break;
               }
             }
           }
           this.sprintsOfProjet=resultat;
        }
      }
}
