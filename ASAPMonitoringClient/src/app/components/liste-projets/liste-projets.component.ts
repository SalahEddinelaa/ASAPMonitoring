import { Component, OnInit } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { ProjetService} from '../../providers/projet.service';
import { Projet, Utilisateur} from '../../data/entities';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { Router } from '@angular/router';
import {RouterModule, Routes} from '@angular/router';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';




declare let $ :any;

@Component({
  selector: 'app-liste-projets',
  templateUrl: './liste-projets.component.html',
  styleUrls: ['./liste-projets.component.css']
})
export class ListeProjetsComponent implements OnInit {
  projets : any;
  allProjetsList:any;
  responsables : any;
  idprojet:string;
  id : string;
  listusers : any;
  listprojets : any;
  allProjetSearch : any;
  prj : Projet;
  resp: Utilisateur;
  idProjet : string;
  itemsOfPage:number=5;
  page: any;
  
 
  constructor(public  projetService : ProjetService , public utilisateurservice : UtilisateurService , private router : Router,
    public auth:AuthenticationServiceService) { }

  ngOnInit() {
    this.getAllProjets();
    
  }
  getAllProjets(){

    this.projetService.getAllProjets().subscribe(data=>{
     this.projets=data;
    for(let p of this.projets)
    {
      this.projetService.getRespOfProjet(p.idProjet).subscribe(data =>{
        p.responsable=data;
      });
    }
    this.allProjetSearch=data;
    this.allProjetsList=data;
 
    });
    }



    getRespOfProjet(idProjet:String){
      this.projetService.getRespOfProjet(idProjet).subscribe(data =>{this.resp=data;})
    }

    deleteprojet() 
  {
    this.projetService.deleteProjet(this.idprojet).subscribe(()=>{
      this.getAllProjets();
    }); 
  }



  moreDetails(projet : any){
    $("#codeAP").html("code AP : "+ projet.codeAP);
    $("#nomProjet").html("Nom projet : "+ projet.nomProjet);
    $("#date_debut").html("Date début : "+ projet.date_debut);
    $("#date_fin").html("Date fin : "+ projet.date_fin);
   
  
    $("#details").modal("show")
  }
  
 details(idProjet : string){
   this.router.navigate(['DetailsProjet'],{queryParams : {"id": idProjet}});
 }  

 createProject(){
   this.router.navigate(['AjouterProjet'])
 }

 Affecter(idProjet : string){
  this.router.navigate(['AffecterEquipe'],{queryParams : {"id": idProjet}});
} 

 searchAnimation(){ 
  $(".iconesearch").css("border-color","#04B404");
  $(".searchinput").css("border-color","#04B404");
}

searchProjet(motCle:string)
  {
    var resultat:Projet[]=[];
    if(motCle == "")
    {
           this.getAllProjets();
    }
    else{

       for(let projet of this.allProjetSearch)
       {
         if((projet.codeAP.includes(motCle))||(projet.nomProjet.includes(motCle))||(projet.date_debut.includes(motCle))||(projet.date_fin.includes(motCle)))
         {
           resultat.push(projet);
         }
       }
       this.allProjetsList=resultat;
    }
  }

}

  