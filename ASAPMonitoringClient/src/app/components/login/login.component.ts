import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationFormsService } from '../../providers/validation-forms.service';
import { Router } from '@angular/router';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm;
  isAuhtenticated=0;

  constructor(private formBuilder: FormBuilder,private auth:AuthenticationServiceService , private router : Router) { 
    this.loginForm = this.formBuilder.group({
      'email': ['', [Validators.required,ValidationFormsService.emailValidator]],
      'password': ['', Validators.required]
    });
    
  }

  ngOnInit() {
  }

  Login(){
    var data={"email":this.loginForm.value.email,"password":this.loginForm.value.password};
    this.auth.login(data).subscribe(response=>{
      let jwtToken=response.headers.get("Authorization");
      this.auth.saveToken(jwtToken);
      this.router.navigate(["/home/allproject"]);
         },
     err=>{
          this.isAuhtenticated=0;
     }
   );
  }

  logout(){
    this.auth.logout();
    this.isAuhtenticated=0;
   // this.router.navigateByUrl("/login");
  }

}
