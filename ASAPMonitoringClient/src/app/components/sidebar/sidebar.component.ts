import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';
import { Router } from '@angular/router';
import { Utilisateur } from '../../data/entities';
import { UtilisateurService } from '../../providers/utilisateur.service';
import { Observable } from 'rxjs/Observable';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

declare let $ :any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  currentUser:Observable<Utilisateur>;
  private serverUrl = 'http://localhost:8080/asapendpoint'
  private stompClient;

  constructor(public auth:AuthenticationServiceService,private userService:UtilisateurService,private router : Router) { 
     this.initializeWebSocketConnection();
  }

  ngOnInit() {
    $(".sidebar-dropdown > a").click(function () {
      $(".sidebar-submenu").slideUp(200);
      if ($(this).parent().hasClass("active")) {
          $(".sidebar-dropdown").removeClass("active");
          $(this).parent().removeClass("active");
      } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this).next(".sidebar-submenu").slideDown(200);
          $(this).parent().addClass("active");
      }

  });

  $("#toggle-sidebar").click(function () {
      $(".page-wrapper").toggleClass("toggled");
  });
    if(!this.auth.isAuthenticated()){
           this.router.navigateByUrl("");
    }else{
      var username=this.auth.loadAuthenticatedUsername();
      this.currentUser = this.userService.findUserByUsername(username);
      this.router.navigate(["./allproject"]);
    }
  }

  logout(){
    this.auth.logout();
    this.router.navigateByUrl("");
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe("/user/app/topic", (message) => {
        if(message.body) {
        //  $(".chat").append("<div class='message'>"+message.body+"</div>")
          console.log(message.body);
        }
      });
    });
  }

}

