import { Component, OnInit, Input, OnDestroy, ViewChild, SimpleChanges, OnChanges } from '@angular/core';
import { Team, Developpeur } from '../../../data/entities';
import { TeamsService } from '../../../providers/teams.service';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, merge , filter} from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { of } from 'rxjs/observable/of';
import { Router } from '@angular/router';
import { AuthenticationServiceService } from '../../../providers/authentication-service.service';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit,OnDestroy,OnChanges {
  @Input()
  selectedTeam: Team;

  @Input()
  parentSubject: Subject<any>;

  developers$: Observable<Developpeur[] >;
  
  developersForm: FormGroup ;
  model: any ;
  searching = false; searchFailed = false;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);
  focus$ = new Subject<string>(); click$ = new Subject<string>();
  formatMatches = (value: any) => value.nom + ' ' + value.prenom || '';
  @ViewChild('instance') instance: NgbTypeahead;
  
  constructor(private fb: FormBuilder , private teamsService: TeamsService , private router : Router,public auth:AuthenticationServiceService) { 
    this.developersForm = this.fb.group ({
      usernames : this.fb.array([])
    })
  }

  ngOnInit() {
    this.parentSubject.subscribe(
      (team) => {
      this.selectedTeam =team ;
      this.developers$ = this.getTeamDevelopers(this.selectedTeam.idEquipe);
  } , 
  (err) => this.router.navigateByUrl("/signin")
 );
}

  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.clearFormArray(this.usernames);
  }

  getTeamDevelopers(idEquipe: string ):Observable<Developpeur[]> {
    return this.teamsService.getTeamDevelopers(idEquipe) ;
     }   ;

  onDeleteUser = (idUser: string , idTeam: string) => {
     this.teamsService.deleteMemberFromTeam(idUser,idTeam).subscribe(
       () => {
        this.developers$ = this.getTeamDevelopers(this.selectedTeam.idEquipe);
       } ,
       (err) => this.router.navigateByUrl("/signin")
      );
    }

  createInput(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
    });
  }

  get usernames() { return this.developersForm.get('usernames') as FormArray  }
  
  addUser = () => { this.usernames.push(this.createInput()); }

  onRemoveUser = (index: number) => { this.usernames.removeAt(index) ;}

  search = (text$: Observable<string>) =>
    text$.pipe( debounceTime(300), distinctUntilChanged(), merge(this.focus$),
     merge(this.click$.pipe(filter(() => !this.instance.isPopupOpen()))),
      tap(() => this.searching = true), switchMap(term =>
        this.teamsService.searchTeams(term,this.selectedTeam.idEquipe).pipe(
          tap(() => (this.searchFailed = false)), 
          catchError(() => {
            this.searchFailed = true;
             return of([]);  })) ),
      tap(() => this.searching = false),
      merge(this.hideSearchingWhenUnsubscribed)
    );

  clearFormArray = (formArray: FormArray) => {
      while (formArray.length !== 0) 
          formArray.removeAt(0)
    }

  selectedItem(item){
      let user=item.item;      
      this.teamsService.addMember(this.selectedTeam.idEquipe,user).subscribe(
        () =>  this.developers$ = this.getTeamDevelopers(this.selectedTeam.idEquipe) ,
       );
     }

    }

