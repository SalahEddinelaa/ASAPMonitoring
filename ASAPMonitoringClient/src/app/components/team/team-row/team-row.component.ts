import { Component, OnInit, Input, Output , EventEmitter, OnChanges, SimpleChange, ViewChild, SimpleChanges} from '@angular/core';
import { FormControl , FormGroupDirective, NgForm, FormBuilder , FormGroup, Validators } from '@angular/forms';
import { Team, Utilisateur } from '../../../data/entities';
import { TeamsService } from '../../../providers/teams.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import {catchError, debounceTime, distinctUntilChanged, map, tap, switchMap, merge , filter} from 'rxjs/operators';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs/Subject';
import { INVALID, FormArray } from '@angular/forms/src/model';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationServiceService } from '../../../providers/authentication-service.service';

@Component({
  selector: 'app-team-row',
  templateUrl: './team-row.component.html' ,
  styleUrls: ['./team-row.component.css']
})
export class TeamRowComponent {
  teamForm: FormGroup ;
  disableBtn : boolean ;
  // NGB-TYPEAHEAD VARIABLES
  model: any ;
  searching = false; searchFailed = false;
  hideSearchingWhenUnsubscribed = new Observable(() => () => this.searching = false);
  focus$ = new Subject<string>(); click$ = new Subject<string>();
  formatMatches = (value: any) => value.nom + ' ' + value.prenom || '';
  @ViewChild('instance') instance: NgbTypeahead;

  showMinus: boolean = false ;
  team: Team = {
    nomEquipe: '',
  };

  developersArray : Utilisateur[] = [];

  constructor(private fb: FormBuilder , private teamsService: TeamsService , private router: Router,public auth:AuthenticationServiceService) { 
    this.teamForm = this.fb.group ({
      name: new FormControl('', [
        Validators.required,  ]) , 
      usernames : this.fb.array([])
    })
  }

  createInput(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
    });
  }
  
     addInput(): void {
    this.usernames.push(this.createInput());
  }

  search = (text$: Observable<string>) =>
    text$.pipe( debounceTime(300), distinctUntilChanged(), merge(this.focus$),
     merge(this.click$.pipe(filter(() => !this.instance.isPopupOpen()))),
      tap(() => this.searching = true), switchMap(term =>
        this.teamsService.searchTeams(term,null).pipe(
          tap(() => this.searchFailed = false), 
          catchError(() => {
            this.searchFailed = true;
             return of([]);  })) ),
      tap(() => this.searching = false),
      merge(this.hideSearchingWhenUnsubscribed)
    );

    AddUser = () => {
      this.addInput() ;
      this.showMinus = true ;
     }
    
     saveTeam = (content) => {
      if (this.teamForm.dirty && this.teamForm.valid ) {
          this.developersArray = this.usernames.value.map(obj => obj.name);
          this.team = this.prepareSendTeam();
          this.teamsService.addTeam(this.team).subscribe();
          this.clearFormArray(this.usernames);
          this.developersArray = [];
      }
    }

    isDisabled(){
      if(this.teamForm.valid && this.auth.isAdmin()){
        return false;
      }else
      return true;
    }

    //Clear inputs 
    clearFormArray = (formArray: FormArray) => {
      while (formArray.length !== 0) {
        formArray.removeAt(0)
      }
    }

    onRemoveUser = (index: number) => { this.usernames.removeAt(index) ;}

    get name() { return this.teamForm.get('name'); }
  
    onDelete() {  
      this.clearFormArray(this.usernames);
      this.developersArray = [] ;
    }
  
    prepareSendTeam(): Team {
      const formModel = this.name.value;
      const saveTeam: Team = {
        nomEquipe: formModel as string,
        members: this.developersArray
      };
      return saveTeam;
    }

    get usernames() {
      return this.teamForm.get('usernames') as FormArray 
      }

    }
