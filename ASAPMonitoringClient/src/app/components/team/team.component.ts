import { Component, OnInit, Input, ViewChild, AfterViewChecked } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import { Team, Utilisateur } from '../../data/entities';
import {MatButtonModule} from '@angular/material/button';
import { TeamsService } from '../../providers/teams.service';
import { NgbTabChangeEvent, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { TeamRowComponent } from '../team/team-row/team-row.component' ;
import { ActivatedRoute } from '@angular/router';
import { AuthenticationServiceService } from '../../providers/authentication-service.service';

@Component({
  selector: 'app-teams',
  templateUrl: './team.component.html',
  providers: [ TeamsService ],
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
    
  constructor(public auth:AuthenticationServiceService) {
   }
  
  ngOnInit(): void {
    this.addedteams.push(this.firstTeam);
  }
  
  addedteams: Team[] = [];
  firstTeam: Team = {
    'nomEquipe': '',
  };

  recieveTeam(team: Team) {
    this.addedteams.push(team);
  }

  receiveDeleteTeam(teamId: string){
    this.addedteams = this.addedteams.filter(obj => obj.idEquipe !== teamId);
  // this.memberRow.removeUsers(); 
  }

  public beforeChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'tab2') {
      this.addedteams = this.addedteams.filter(obj => obj === this.firstTeam);
    }
  };

  



  

}