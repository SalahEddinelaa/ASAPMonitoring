import { DataSource } from "@angular/cdk/table";
import { TeamsService } from "../../../providers/teams.service";
import { Observable } from "rxjs/Observable";
import { Team } from "../../../data/entities";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { CollectionViewer } from "@angular/cdk/collections";
import { catchError, finalize } from "rxjs/operators";
import { of } from "rxjs/observable/of";

export class TeamDataSource implements DataSource<Team> {
    private teamsSubject = new BehaviorSubject<Team[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    constructor(private teamsService: TeamsService){}

    connect(collectionViewer: CollectionViewer): Observable<Team[]> {
        return this.teamsSubject.asObservable() ;
    }

    disconnect(collectionViewer: CollectionViewer ) {
        this.teamsSubject.complete() ;
        this.loadingSubject.complete();
    } 

    loadTeams( page , size ){
        let rows = [] ;
        this.loadingSubject.next(true);
        this.teamsService.getTeams( page, size)
        .pipe( catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        )
        .subscribe(teams => {
              teams['content'].forEach(element => {
                    rows.push(element ) ;
            });
            this.teamsSubject.next(teams['content']);
            console.log("teams are : " + JSON.stringify(teams['content']));
        });
        
        
        }
    
    }
  