import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { TeamsService } from '../../../providers/teams.service';
import { Team } from '../../../data/entities';
import { MatTableDataSource, MatPaginator , MatSort} from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs/Observable';
import { TeamDataSource } from './team-data-source';
import { tap, merge, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
import { FormArray } from '@angular/forms';
import { AuthenticationServiceService } from '../../../providers/authentication-service.service';



@Component({
  selector: 'app-teams-table',
  templateUrl: './teams-table.component.html',
  styleUrls: ['./teams-table.component.scss'], 
})
export class TeamsTableComponent implements AfterViewInit, OnInit {
  selectedTeam: Team = { } ;
  displayedColumns = ['Nom Equipe', 'Date de Création' , 'Actions' ];
  dataSource : TeamDataSource ;
  totalElements: number ;
  isDetailHidden: boolean ;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  parentSubject:Subject<any> = new Subject();
  isDeleteButtonEnabled: boolean ;
  teams: Team[] = [];
  editedTeam: Team;// the team currently being edited
  constructor(private teamsService: TeamsService,public auth:AuthenticationServiceService){ }

  ngOnInit(): void { 
    this.dataSource = new TeamDataSource(this.teamsService) ;
    this.teamsService.getTeamsLength().subscribe((res) => this.totalElements =res) 
    this.dataSource.loadTeams( 0, 5);
    this.isDetailHidden = true ;
  }

  ngAfterViewInit() {
    this.paginator.page
        .pipe(
            tap(() => this.loadTeamsPage())
        )
        .subscribe();
  }

  loadTeamsPage(){
    this.dataSource.loadTeams( this.paginator.pageIndex,
      this.paginator.pageSize);
  }
    
  updateTeam() {
      if (this.editTeam) {
        this.teamsService.updateTeam(this.editedTeam)
          .subscribe(team => {
            const ix = team ? this.teams.findIndex(h => h.idEquipe === team.idEquipe) : -1;
            if (ix > -1) { this.teams[ix] = team; }
          });
        this.editTeam = undefined;
      }
  }

  editTeam(team) {
      this.editTeam = team;
  }
  
  searchTeam(idTeam:string , searchTerm: string) {
      this.editTeam = undefined;
      if (searchTerm) {
        this.teamsService.searchTeams(idTeam , searchTerm)
          .subscribe(teams =>{
            this.teams = teams;
          })
      }
  }

  onRowClicked = (team) => {
      this.isDetailHidden = false ; 
      this.selectedTeam= team ;  
      this.parentSubject.next(this.selectedTeam);  
  } 

  removeTeam = (teamId: string) => {
    this.teamsService.deleteTeam(teamId).subscribe( () => {
      this.loadTeamsPage() ; 
      this.teamsService.getTeamsLength().subscribe((res) => this.totalElements =res) 
    });      
    }

}

