import { Component, OnInit,Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidationFormsService } from '../../../providers/validation-forms.service';

@Component({
  selector: 'app-validation-message-control',
  template: '<small class="form-text text-danger" *ngIf="_errorMessage !== null">{{errorMessage}}</small>',
})
export class ValidationMessageControlComponent implements OnInit {
  private _errorMessage: string;
  @Input() control: FormControl;

  constructor() { }

  ngOnInit() {
    console.log(this.control.value);
  }

  get errorMessage() {
    for (let propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationFormsService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }
   return null;
  }

}
