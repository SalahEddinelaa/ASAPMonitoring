export class Team {
    idEquipe?: string;
    nomEquipe?: string;
    members?: Utilisateur[];
    weeklyHours?: number;
    createdAt?: string ;
}

export class Utilisateur{
    
    public idUser:string;
    public nom:string;
    public prenom:string;
    public revoke:string;
    public pathPhoto:string;
    public email:string;
    public tel:string;
    public password:string;
    public role:string;
    public photoBase64:string;
    
}


export class Projet{

    public idProjet:string;
    public codeAP:string;
    public nomProjet:string;
    public date_debut:Date;
    public date_fin:Date;
    public responsable:ScrumMaster=new ScrumMaster();

}


export class ScrumMaster extends Utilisateur{

    public smprojets:Projet[]=[];

}

export class Admin extends Utilisateur{

}

export class Developpeur extends Utilisateur{

}

export class Demande{
    public idDemande:string;
    public nomDemande:string;
	public key:string;
	public description:string;
	public estimation:string;
	public storyPoint:string;
	public priority:string;
	public status:string;
    public endDate:Date;
    public responsable:Developpeur;
    public subtasks:Demande[];
    public subtask:Boolean;
}

export class Sprint{

    public idSprint:string;
    public nomSprint:string;
    public date_debut:Date;
    public date_fin:Date;
    public demandes:Demande[];
    public statut:String;
    
}



export class Application{
    public idApplication : string;
    public nomApplication : string;
    public projet : Projet;
}



