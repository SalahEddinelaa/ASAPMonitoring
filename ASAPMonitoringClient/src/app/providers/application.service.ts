import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Projet, Application } from '../data/entities';

@Injectable()
export class ApplicationService {

  constructor(public http:Http) { }


  getAllProjects(idApplication : string){
    return this.http.get("http://localhost:8080/application/"+idApplication)
   .map(res => res.text() ? res.json() : res);;

  }
  getOne(idApplication : string){
    return this.http.get("http://localhost:8080/application/"+idApplication) 
      .map(res => res.text() ? res.json() : res);;
  }

  getAllApplications(){
    return this.http.get("http://localhost:8080/applications")
    .map(res=>res.json());
  }


  addApp(application : Application , projet : string ){
    console.log("add app");
    return this.http.post("http://localhost:8080/application/"+projet,application)
   .map(res => res.text() ? res.json() : res);;
}

getProjetOfApp(idApplication){
  return this.http.get("http://localhost:8080/ProjetOfApplication/"+idApplication)
  .map(res => res.text() ? res.json() : res);;


}

deleteApp(idApplication : string){
  return this.http.delete("http://localhost:8080/deleteApp/"+idApplication)
  .map(res=>console.log("supp"));

}
}
