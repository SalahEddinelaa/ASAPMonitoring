import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {JwtHelper} from 'angular2-jwt';
import { Utilisateur } from '../data/entities';
import { UtilisateurService } from './utilisateur.service';

@Injectable()
export class AuthenticationServiceService {

  private jwtToken;
  private roles;
  private currentUser:Utilisateur;

  constructor(private http:HttpClient) { }

login(user){
    return this.http.post("http://localhost:8080/login",user,{observe:"response"});
   }

isAdmin(){
    for(let item of this.roles){
        if(item.authority=="Administrateur")
        {
            return true;
        }
    }
    return false;
}

isScrumMaster(){
    for(let item of this.roles){
        if(item.authority=="Scrum Master")
        {
            return true;
        }
    }
    return false;
}

saveToken(jwt:string){
    this.jwtToken=jwt;
    localStorage.setItem("token",jwt); 
  }
  
  loadToken():String{
      this.jwtToken=localStorage.getItem("token");
      return this.jwtToken;
  }
  
  loadAuthenticatedUsername():string{
      this.loadToken();
      let helper = new JwtHelper();
      this.roles=helper.decodeToken(this.jwtToken).roles;
      return helper.decodeToken(this.jwtToken).sub;
  }
  
  isAuthenticated(){
      if(localStorage.getItem("token")!=null){
              return true;
      }else{
          return false;
      }
  }
  
  logout(){
      localStorage.removeItem("token");
      this.jwtToken=null;
  }

}
