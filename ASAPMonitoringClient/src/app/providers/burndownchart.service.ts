import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationServiceService } from './authentication-service.service';
import { Sprint } from '../data/entities';


@Injectable()
export class BurndownchartService {
  chartUrl = 'http://localhost:8080/api/chart' ;
  private jwt;

  constructor(private _http:HttpClient,private auth:AuthenticationServiceService) { 
    this.jwt=this.auth.loadToken();
  }


  getChartData(idSprint: number) : Observable<any> {
    const url = `${this.chartUrl}/${idSprint}`; 

    return this._http.get(url,{headers : new HttpHeaders({
      'Authorization' : this.jwt
    })})
    .map(result => result);
    
      }
    }
