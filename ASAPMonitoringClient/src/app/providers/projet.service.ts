import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Projet, ScrumMaster } from '../data/entities';

@Injectable()
export class ProjetService {

  constructor(private http:HttpClient) { }

  allProjets()
  {
    return this.http.get("http://localhost:8080/projets",{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  AddProjet(projet:Projet,responsable:string){
    return this.http.post<Projet>("http://localhost:8080/projets/"+responsable,projet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }


  update(projet:Projet,resp:string){
    return this.http.post("http://localhost:8080/projets/"+resp,projet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  affecterEquipeToProjet(idsEquipes : number , projet : Projet){
    return this.http.post("http://localhost:8080/equipesProjet/"+idsEquipes,projet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }


  getAllProjets(){
    return this.http.get("http://localhost:8080/projets",{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  deleteProjet(idProjet:string)
    {
      return this.http.delete("http://localhost:8080/projets/"+idProjet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
    }

    getOne(idProjet: string) {
      return this.http.get<Projet>("http://localhost:8080/projets/"+idProjet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})}) ;
    }

    getAppOfProjet(idProjet){
      return this.http.get("http://localhost:8080/projetApps/"+idProjet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
    }

    getRespOfProjet(idProjet){
      return this.http.get<ScrumMaster>("http://localhost:8080/projets/scrumMaster/"+idProjet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
    }
   
  projetsScrumMasterFiltre(idUser:string)
  {
    return this.http.get("http://localhost:8080/projets/scrumMaster/"+idUser,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

}
