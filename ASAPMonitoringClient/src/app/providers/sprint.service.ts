import { Injectable } from '@angular/core';
import { Sprint, Developpeur } from '../data/entities';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class SprintService {

  constructor(private http:HttpClient) { }


  allDemandes(){
    return this.http.get("http://localhost:8080/demandes",{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  allSprintsOfProjet(idProjet:string){
    return this.http.get<Sprint[]>("http://localhost:8080/sprints/projet/"+idProjet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  createSprint(sprintName:string,idProjet:string){
    return this.http.get("http://localhost:8080/sprints/"+sprintName+"/create/"+idProjet,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  deleteSprint(idsprint:string){
    return this.http.delete("http://localhost:8080/sprints/"+idsprint,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  getOne(idsprint:string){
    return this.http.get("http://localhost:8080/sprints/"+idsprint,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }

  affectResponsableToDemande(idresponsable:string,iddemande:string){
    return this.http.get<Developpeur>("http://localhost:8080/sprints/demandes/"+iddemande+"/responsable/"+idresponsable,{headers:new HttpHeaders({'Authorization':localStorage.getItem("token")})});
  }


}
