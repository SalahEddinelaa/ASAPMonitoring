
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/do';


import { Team, Utilisateur, Developpeur} from '../data/entities';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { AuthenticationServiceService } from './authentication-service.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable()
export class TeamsService {

  teamsUrl = 'http://localhost:8080/api/teams';
  deleteUrl = 'http://localhost:8080/api/team';
  teamAddUser = 'http://localhost:8080/api/teams/addmember' ;
  usersSearchUrl = 'http://localhost:8080/utilisateurs/filter?searchTerm=';
  teamsLength = 'http://localhost:8080/api/teams/length';
  teamsSearch = 'http://localhost:8080/api/teams/term';
  private handleError: HandleError;
  private jwt;

  constructor(private http: HttpClient, private httpErrorHandler: HttpErrorHandler,private auth:AuthenticationServiceService) {
    this.handleError = httpErrorHandler.createHandleError('Teams');
    this.jwt=this.auth.loadToken();
  }

  searchTeams(searchTerm: string , idTeam:string): Observable<Team[]> {
    if (searchTerm === '') {
      return of([]);
    }
    return this.http.get<Team[]>(this.usersSearchUrl+searchTerm+"&idTeam="+idTeam ,{headers:new HttpHeaders({'Authorization':this.jwt})});
  }

  search(searchTerm: string): Observable<Team[]> {
    if (searchTerm === '') {
      return of([]);
    }
    return this.http.get<Team[]>(this.usersSearchUrl+searchTerm,{headers:new HttpHeaders({'Authorization':this.jwt})});
  }
  
  
  addTeam(team: Team): Observable<Team> {
    return this.http.post<Team>(this.teamsUrl, team,{headers:new HttpHeaders({'Authorization':this.jwt})})
      .pipe(
        catchError(this.handleError('addTeam', team))
      );
  }

  getTeams ( page = 0, size = 5): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamsUrl,  { params: new HttpParams()
        .set('page', page.toString())
        .set('size', size.toString()) ,
        headers : new HttpHeaders({
          'Authorization' : this.jwt
        })})
      .pipe(
        catchError(this.handleError('getTeams', []))
      );
  }


  getTeamsLength (): Observable<number> {
    return this.http.get<number>(this.teamsLength , {headers : new HttpHeaders({
      'Authorization' : this.jwt
    })});
  }

  deleteTeam (teamId: string): Observable<Team> {
    const url = `${this.teamsUrl}/${teamId}`; 
    return this.http.delete(url, {headers : new HttpHeaders({
      'Authorization' : this.jwt
    })})
      .pipe(
        catchError(this.handleError('deleteTeam'))
      );
  }

  updateTeam (team: Team): Observable<Team> {
    return this.http.put<Team>(this.teamsUrl, team, {headers : new HttpHeaders({
      'Authorization' : this.jwt
    })})
      .pipe(
        catchError(this.handleError('updateTeam', team))
      );
  }

  getTeamDevelopers(teamId: string) : Observable<Developpeur[]> {
    const url = `${this.teamsUrl}/${teamId}`; 
    return this.http.get<Developpeur[]>(url, {headers : new HttpHeaders({
      'Authorization' : this.jwt
    })}) ;
  }

  deleteMemberFromTeam(teamId: string , userId: string){
    return this.http.delete<Team[]>(this.deleteUrl,  { params: new HttpParams()
      .set('teamId', teamId)
      .set('userId', userId)
      , headers : new HttpHeaders({
        'Authorization' : this.jwt 
      })}).pipe(
        catchError(this.handleError('deleteMemberFromTeam'))
      ); ; 
    }

  addMember(idTeam:string , developpeur: Developpeur): Observable<Developpeur> {
      let request = { developpeur:developpeur , idTeam :idTeam}
      return this.http.post<Developpeur>(this.teamAddUser, request , {headers : new HttpHeaders({
        'Authorization' : this.jwt
      })} )
        .pipe(
          catchError(this.handleError('addDeveloppeur', developpeur))
        );
  }

  desaffecterEquipe(idProjet: string , idEquipe : string){
    return this.http.get("http://localhost:8080/api/desaffectation/"+idProjet+"/"+idEquipe,{headers : new HttpHeaders({
      'Authorization' : this.jwt
    })} );

  }

  getEquipesNonAffectees(idProjet: string):Observable<Team[]> {
    return this.http.get<Team[]>("http://localhost:8080/api/equipesNonAffectees/projet/"+ idProjet,{headers : new HttpHeaders({
      'Authorization' : this.jwt
    })} );
    }
  
  getEquipesOfProjet(idProjet: string) :Observable<Team[]>{
    console.log('EquipesOfProject Get request to back initiated...');
    return this.http.get<Team[]>("http://localhost:8080/api/equipes/projet/" + idProjet,{headers : new HttpHeaders({
      'Authorization' : this.jwt
    })} );
  }

}
