import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Utilisateur} from '../data/entities';
import { Observable } from 'rxjs/Observable';
import { AuthenticationServiceService } from './authentication-service.service';

@Injectable()
export class UtilisateurService {

  private jwt;

  constructor(private http: HttpClient,private auth:AuthenticationServiceService) {
      this.jwt=this.auth.loadToken();
  }
 
  ToStorage(file:File,user:Utilisateur) {

   let formdata: FormData = new FormData();
    formdata.append('file', file);
    Object.keys(user).forEach(key => {
      formdata.append(key, user[key]);
    }); 
    
    return this.http.post<Utilisateur>("http://localhost:8080/utilisateurs/",formdata,{headers:new HttpHeaders({'Authorization':this.jwt})});
  }

  listeSM(){
    return this.http.get("http://localhost:8080/utilisateurs/scrumMasters",{headers:new HttpHeaders({'Authorization':this.jwt})});
  }

 allUsers()
 {
    return this.http.get("http://localhost:8080/utilisateurs",{headers:new HttpHeaders({'Authorization':this.jwt})});
 }

 deleteUser(idUser:string)
 {
   return this.http.delete("http://localhost:8080/utilisateurs/"+idUser,{headers:new HttpHeaders({'Authorization':this.jwt})});
 }

getUtilisateurById(idUser)
{
  return this.http.get<Utilisateur>("http://localhost:8080/utilisateurs/"+idUser,{headers:new HttpHeaders({'Authorization':this.jwt})});
}

GetProjetsOfDevelopper(idUser)
{
  return this.http.get("http://localhost:8080//utilisateurs/developper/"+idUser+"/projets",{headers:new HttpHeaders({'Authorization':this.jwt})});
}

updateUser(User:Utilisateur)
{
  return this.http.post<Utilisateur>("http://localhost:8080/utilisateurs/profil",User,{headers:new HttpHeaders({'Authorization':this.jwt})});
}

updateUserPhoto(file: File,id:string)
{
  let formdata: FormData = new FormData();
  formdata.append('file', file);
  return this.http.post<Utilisateur>("http://localhost:8080/utilisateurs/profil/"+id,formdata,{headers:new HttpHeaders({'Authorization':this.jwt})});
}

deleteProjetFromListUser(idUser:string,idProjet:string){
  return this.http.delete("http://localhost:8080/utilisateurs/projets/"+idUser+"/"+idProjet,{headers:new HttpHeaders({'Authorization':this.jwt})});
}

//affecter un projet au utilisateur
affecterProjetToUser(idUser:string,idProjets:any)
{
  return this.http.post("http://localhost:8080/utilisateurs/projets/affectation/"+idUser,idProjets,{headers:new HttpHeaders({'Authorization':this.jwt})});
}

//get developpers of a project
getDeveloppersOfProject(idprojet:string){
  return this.http.get("http://localhost:8080/utilisateurs/developpers/projet/"+idprojet,{headers:new HttpHeaders({'Authorization':this.jwt})});
}

findUserByUsername(username:string):Observable<Utilisateur>{
  return this.http.get<Utilisateur>("http://localhost:8080/utilisateurs/username/"+username,{headers:new HttpHeaders({'Authorization':this.jwt})});
}
  
}


