import { Injectable } from '@angular/core';

@Injectable()
export class ValidationFormsService {

  constructor() { }

  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
        'required': 'Champ obligatoire',
        'invalidEmailAddress': 'Adresse mail invalide!!',
        'invalidPassword': 'votre mot de passe doit contenir au moins un nombre',
        'minlength': `Minimum length ${validatorValue.requiredLength}`,
        'invalidRole' :'veuillez choisir un rôle',
        'invalidDates':'Date de fin doit être supérieur à la date de début'
    };

    return config[validatorName];
}

static emailValidator(control) {
    // RFC 2822 compliant regex
    if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
        return null;
    } else {
        return { 'invalidEmailAddress': true };
    }
}

static passwordValidator(control) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,50}$/)) {
        return null;
    } else {
        return { 'invalidPassword': true };
    }
}

static roleValidator(control){
    if(control.value=="0"){
        return {'invalidRole': true};
    }else{
        return null;
    }
}

static emailExists(control){
    
}

static datesValidation(control){

  if(control.parent && control.value!=null){
    var date_debut=control.parent.controls.date_debut.value;
    var date_fin=control.value;
    let dateDebutFormat = new Date(date_debut.year, date_debut.month, date_debut.day);
    let dateFinFormat = new Date(date_fin.year, date_fin.month, date_fin.day);
    if(dateDebutFormat>dateFinFormat){
        return {'invalidDates':true}
    }else{
        return false;
    }
  }
 
}


}


